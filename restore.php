<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require 'ch_debug.php';
require 'config_cron.php';

$sql = "RESTORE DATABASE ka-msftsql FROM DISK = '$backup_file' WITH RECOVERY";
ch_debug($sql);
$stmt = sqlsrv_query($conn, $sql);
if ($stmt === false)
    ch_debug(print_r(sqlsrv_errors()));
else
    ch_debug("Database restored from $backup_file</br>");

//Put DB into usable state.
$sql = "USE ka-msftsql";
ch_debug($sql);

$stmt = sqlsrv_query($conn, $sql);

if ($stmt === false)
    ch_debug(print_r(sqlsrv_errors()));
else
    ch_debug("Using ka-msftsqlDB</br>");