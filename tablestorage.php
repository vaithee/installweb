<html>
    <head></head>
    <body>
        <?php
        require 'WindowsAzure\Table\ITable.php';
        require 'entities.php';

        $storageClient = new Microsoft_WindowsAzure_Storage_Table('table.core.windows.net', 'iminteractiveinstall2199', '77uYvNagWxCt5V2JTsk+FCAoADdx+O1NoNrHLPXdX8LQjr9mmj+8tiAQ33bONA304QcRT8OEtZun1oaqC0BEYw==');
        if (!$storageClient->tableExists("ContactsTable"))
            $storageClient->createTable("ContactsTable");

        if (!isset($_GET['action'])) {
            PrintForm('addContact', 'business');
            FilterContacts(null, $storageClient);
        } else {
            try {
                switch ($_GET['action']) {
                    case 'addContact':
                        if ($_POST['email'] == "")
                            echo "E-mail address is required.";
                        else {
                            $contact = new Contact($_POST['contacttype'], $_POST['email']);
                            $contact->Name = $_POST['name'];
                            $contact->Address = $_POST['address'];
                            $contact->Phone = $_POST['phone'];
                            $result = $storageClient->insertEntity('ContactsTable', $contact);
                            PrintForm('addContact', 'business');
                            FilterContacts(null, $storageClient);
                        }
                        break;
                    case 'editContact':
                        $contact = $storageClient->retrieveEntityById('ContactsTable', $_GET['contacttype'], $_GET['email']);
                        PrintForm('updateContact', $_GET['contacttype'], $_GET['email'], $contact->Name, $contact->Address, $contact->Phone);
                        FilterContacts(null, $storageClient);
                        break;
                    case 'updateContact':
                        // Partition keys and row keys cannot be updated, so delete the old entity and create a new one.
                        if ($_POST['oldcontacttype'] != $_POST['contacttype'] || $_POST['oldemail'] != $_POST['email']) {
                            // Delete the old entity.
                            $oldContact = $storageClient->retrieveEntityById('ContactsTable', $_POST['oldcontacttype'], $_POST['oldemail']);
                            $storageClient->deleteEntity('ContactsTable', $oldContact);

                            // Create a new entity.
                            $contact = new Contact($_POST['contacttype'], $_POST['email']);
                            $contact->Name = $_POST['name'];
                            $contact->Address = $_POST['address'];
                            $contact->Phone = $_POST['phone'];
                            $result = $storageClient->insertEntity('ContactsTable', $contact);
                        } else { //If only custom properties are changed, the entity can be updated.
                            $contact = $storageClient->retrieveEntityById('ContactsTable', $_POST['contacttype'], $_POST['email']);
                            $contact->Name = $_POST['name'];
                            $contact->Address = $_POST['address'];
                            $contact->Phone = $_POST['phone'];
                            $storageClient->updateEntity('ContactsTable', $contact);
                        }
                        PrintForm('addContact', 'business');
                        FilterContacts(null, $storageClient);
                        break;
                    case 'filterContacts':
                        PrintForm('addContact', 'business');
                        $query = "PartitionKey eq '" . $_POST['contacttype'] . "'";
                        FilterContacts($query, $storageClient);
                        break;
                    case 'deleteContact':
                        $contact = $storageClient->retrieveEntityById('ContactsTable', $_GET['contacttype'], $_GET['email']);
                        $storageClient->deleteEntity('ContactsTable', $contact);
                        PrintForm('addContact', 'business');
                        FilterContacts(null, $storageClient);
                        break;
                }
            } catch (Microsoft_WindowsAzure_Exception $e) {
                print("<pre>");
                print_r($e);
                print("</pre>");
            }
        }

        function FilterContacts($query, $storageClient) {
            try {
                print("<form method='post' action='?action=filterContacts' enctype='multipart/form-data'>
				Business <input type='radio' name='contacttype' value='business' > &nbsp;&nbsp;&nbsp;
				Personal <input type='radio' name='contacttype' value='personal'> &nbsp;&nbsp;&nbsp;
				<input type='submit' value='Filter'/></form>
			");

                $contacts = $storageClient->retrieveEntities("ContactsTable", $query, "Contact");
                print("<table border='1px'><tr><td>Name</td><td>Email</td><td>Address</td><td>Phone</td><td>Contact Type</td><td>Action</td></tr>");
                foreach ($contacts as $contact) {
                    print("<tr><td>" . $contact->Name . "</td>");
                    print("<td>" . $contact->getRowKey() . "</td>");
                    print("<td>" . $contact->Address . "</td>");
                    print("<td>" . $contact->Phone . "</td>");
                    print("<td>" . $contact->getPartitionKey() . "</td>");
                    print("<td><a href='?action=editContact&contacttype=" . $contact->getPartitionKey() . "&email=" . $contact->getRowKey() . "'>Edit</a></br>");
                    print("<a href='?action=deleteContact&contacttype=" . $contact->getPartitionKey() . "&email=" . $contact->getRowKey() . "'>Delete</a></td>");
                    print("</tr>");
                }
            } catch (Microsoft_WindowsAzure_Exception $e) {
                print("<pre>");
                print_r($e);
                print("</pre>");
            }
        }

        function PrintForm($action, $contacttype, $email = null, $name = null, $address = null, $phone = null) {
            print("<form method='post' action='?action=" . $action . "' enctype='multipart/form-data'>");
            if ($contacttype == 'business') {
                print("Business <input type='radio' name='contacttype' value='business' checked> &nbsp;&nbsp;&nbsp;
			   Personal <input type='radio' name='contacttype' value='personal'> </br>");
            } else {
                print("Business <input type='radio' name='contacttype' value='business' > &nbsp;&nbsp;&nbsp;
			   Personal <input type='radio' name='contacttype' value='personal' checked> </br>");
            }
            print(" Email <input type='text' name='email' value='" . $email . "'/></br>
			Name <input type='text' name='name' value='" . $name . "'/></br>
			Address <input type='text' name='address' value='" . $address . "'/></br>
			Phone <input type='text' name='phone' value='" . $phone . "'/></br>
		");
            if ($action == 'addContact')
                print("<input type='submit' value='Add Contact'/></form>");
            else
                print("<input type='hidden' name='oldcontacttype' value='" . $contacttype . "'/>
				<input type='hidden' name='oldemail' value='" . $email . "'/>
				<input type='submit' value='Update Contact'/></form>
		");
        }
        ?>
    <body>
</html>