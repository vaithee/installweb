<!DOCTYPE html>
<head>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/ico" href="images/favicon.ico">
    <link rel="icon" type="image/x-icon" href="images/favicon.ico">
    <link rel="icon" type="image/png" href="images/favicon.ico">
    <script src="js/jquery-2.2.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="css/fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="js/moment.min.js" type="text/javascript"></script>
    <script src="js/daterangepicker.js" type="text/javascript"></script>
    <script src="js/date.min.js" type="text/javascript"></script>
    <link href="css/newcss.css" rel="stylesheet" type="text/css">

    <style>
        div[role="button"]:hover{color:#337ab7;}
        div[role="button"] div:first-child::before {
            font-family: 'Glyphicons Halflings';
            content: "\e252";
            margin-right: 3px;
            color:#337ab7;
        }
        div.collapsed[role="button"] div:first-child::before {
            font-family: 'Glyphicons Halflings';
            content: "\e250";
            margin-right: 3px;
            color:#337ab7;
        }
        .list .col-sm-12{border-top: 1px solid #999;border-radius: 1em 0 0 1em; background-color: rgba(128,128,128,.1)}
        .row.collapse{padding:0 0 0 1em;}
        .col-sm-12 .row{padding-top:.3em;padding-bottom:.2em}
        .results {
            position:relative;
        }
        .no_record {
            position: absolute;
            top: 30;
            left: 0;
            color: #A9A9A9;
        }
        .no_record1 {
            position: absolute;
            top: 30;
            left: 0;
            color: #A9A9A9;
        }
        thead{
            font-weight: bolder;
        }
        a#export:hover {
            text-decoration: none;
        }
    </style>
</head>
<body>
    <?php
//    require_once('/simplesamlphp/lib/_autoload.php');
//    $as = new SimpleSAML_Auth_Simple('default-sp');
//    $as->requireAuth();
//    $attributes = $as->getAttributes();
    ?>
    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><span>product software management console</span></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="prod-owner.php">Products</a>
                    </li>
                    <li class="active">
                        <a href="reports.php">Reports</a>
                    </li>
                    <!--                    <li>
                                            <a href="module.php/core/authenticate.php?as=default-sp&logout"><i class="fa fa-power-off" aria-hidden="true" style="font-size: 20px;"></i></a>
                                        </li>-->
                </ul>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Reports</h1>
                    <p>Reports on download counts by software title and version.</p>
                </div>
            </div>
            <div class="row form">
                <div class="col-md-2 form-group">
                    <label for="daterange">Date Range</label>
                    <input class="form-control input-sm" type="text" id="daterange" name="daterange" value="" />

                    <script type="text/javascript">
                        $(function () {
                            $('input[name="daterange"]').daterangepicker();
                        });
                    </script>
                </div>
                <div class="col-md-4 form-group">
                    <label for="exampleInputName2">Search / filter</label>
                    <input type="text" class="form-control input-sm search_criteria" id="search_criteria" placeholder="">
                </div>
                <div class="col-md-2 form-group">
                    <label>Type</label>
                    <select class="form-control input-sm" id="category">
                        <option value="">All</option>
                        <?php
                        include 'config.php';
                        $sql = "SELECT DISTINCT category FROM [ciotiswd].product_owner_list_content";
                        $result = sqlsrv_query($conn, $sql);
                        while ($row = sqlsrv_fetch_array($result)) {
                            ?>
                            <option value="<?php echo $row['category']; ?>"><?php echo $row['category']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-2 form-group">
                    <label>Scanner Model</label>
                    <select class="form-control input-sm product_name" id="product_name" >
                        <option value="">All</option>
                        <?php
                        $sql = "SELECT DISTINCT product_name FROM [ciotiswd].product_owner_list";
                        $result = sqlsrv_query($conn, $sql);
                        while ($row = sqlsrv_fetch_array($result)) {
                            ?>
                            <option value="<?php echo $row['product_name']; ?>"><?php echo $row['product_name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-2">
                    <label>&nbsp;</label>
                    <a id="export"><button  class="btn btn-sm btn-block btn-primary export">Export (.csv)</button></a>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    Title
                </div>
                <div class="col-md-3 text-right">
                    Requests
                </div>
                <div class="col-md-3 text-right">
                    Downloads
                </div>
                <div class="col-md-3 text-right">
                    Attach Rate
                </div>
            </div>

            <div id="results" class="results">

            </div>

        </div>
    </div>

    <script type='text/javascript'>
        $(document).ready(function () {
            $("#results").load("reportajaxnew.php");
            $("#results").bind("DOMSubtreeModified", function () {
                //$(".req:contains('0')").closest(".row.list").hide();
                var norecord = $(".no_record").text();
                if (norecord == "NO RECORDS FOUND")
                {
                    $(".export").attr("disabled", true);
                } else {
                    $(".export").attr("disabled", false);
                }
                $('.hide_accordion:empty').closest(".row.list").hide();
            });
            function exportTableToCSV($table, filename) {
                var $headers = $table.find('tr:has(th)')
                        , $rows = $table.find('tr:has(td)')
                        , tmpColDelim = String.fromCharCode(11)
                        , tmpRowDelim = String.fromCharCode(0)
                        , colDelim = '","'
                        , rowDelim = '"\r\n"';
                var csv = '"';
                csv += formatRows($headers.map(grabRow));
                csv += rowDelim;
                csv += formatRows($rows.map(grabRow)) + '"';
                // Data URI
                var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);
                $(this)
                        .attr({
                            'download': filename
                            , 'href': csvData
                        });
                function formatRows(rows) {
                    return rows.get().join(tmpRowDelim)
                            .split(tmpRowDelim).join(rowDelim)
                            .split(tmpColDelim).join(colDelim);
                }
                function grabRow(i, row) {

                    var $row = $(row);
                    var $cols = $row.find('td');
                    if (!$cols.length)
                        $cols = $row.find('th');
                    return $cols.map(grabCol)
                            .get().join(tmpColDelim);
                }
                function grabCol(j, col) {
                    var $col = $(col),
                            $text = $col.text();
                    return $text.replace('"', '""');
                }
            }
            $("#export").click(function (event) {
                var date = new Date();
                var time = date.toString("yyyyMMddhms");
                var outputFile = 'reports';
                outputFile = outputFile.replace('.csv', '') + "-" + time + '.csv'
                exportTableToCSV.apply(this, [$('#dvData > table'), outputFile]);
            });
        });
    </script>

    <?php include_once("search-code.php"); ?>  

</body>
</html>
