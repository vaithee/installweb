var base_path = location.protocol + "//" + document.domain + "/" + location.pathname.split('/')[1] + "/";
var lang_list = [];
var url;
function downloadFile(url) {
    url = url;
    $('.download_icon').each(function () {
        if ($(this).attr("onclick").match(/'(.*?)'/)[1] == url) {
            setCookie("fileDownload", true);
            $(this).attr('disabled', true);
            $(this).find(".down_load_icon").addClass('glyphicon-refresh glyphicon-spin');
            $(this).attr('data-original-title', "download in progress");

            $.fileDownload(url, {
                successCallback: function (url) {
                    $('.download_icon').each(function () {
                        if ($(this).attr("onclick").match(/'(.*?)'/)[1] == url) {
                            $(this).attr('disabled', true);
                            $('.tooltip').hide();
                            $(this).delay(500).queue(function (next) {
                                $(this).find(".down_load_icon").removeClass('glyphicon-refresh glyphicon-spin glyphicon-download').addClass('glyphicon glyphicon-ok');
                                $(this).css({'background-color': '#95D195', 'border-color': '#95D195'}).attr('data-original-title', "download complete");
                                next();
                            });
                        }
                    });
                },
                failCallback: function (responseHtml, url) {
                    alert($(responseHtml).text());
                }
            });
            return false;
        }
    });
}
function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
$(document).ready(function () {
    //$("#product-status").hide();
    $(".download_icon").click(function () {
        $.fn.ignore = function (sel) {
            return this.clone().find(sel || ">*").remove().end();
        };
        var prod_id = $('#prodId').val();
        var filename = $(this).parent().prev().ignore("span").text().trim();
        var download_date = new Date();
        var date = download_date.toString("yyyy-MM-dd");
        var download_count = 1;
        $.ajax({
            type: "POST",
            url: "download.php",
            data: {filename: filename, date: date, download_count: download_count, prod_id: prod_id},
            success: function (data) {
//                console.log(data);
            }
        });
    });

    $("#lang_dd").change(function () {
        var lang = $(this).val();
        $.ajax({
            type: "POST",
            url: "header.php",
            data: {lang: lang},
            success: function (data) {
                //alert(data);
            }
        });
    });

    $("#lang_dd").change(function () {
        var lang = $(this).val();
        $.ajax({
            type: "POST",
            url: "emailConfig.php",
            data: {lang: lang},
            success: function (data) {
                //alert(data);
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip();

    $.ajax({
        type: "post",
        url: "../getLanguages.php",
        data: $(this).serialize(),
        dataType: "json"
    }).done(function (response) {
        lang_list = response;
        for (var i = 0; i < lang_list.length; i++) {
            $('#lang_dd').append('<option value="' + lang_list[i].language_code + '" >' + lang_list[i].language + '</option>');
        }
    }).error(function (xhr, status, error) {
        var err = eval("(" + xhr.responseText + ")");
        //alert(err.Message);
    });
    $("#popo").popover({
        placement: wheretoplace
    });

    $('#indexForm').validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class"
    });

    $('#indexForm input').on('blur', function () {
        if ($('#indexForm').valid()) {
            $("#inputEmail").removeClass("input_err");
            $("#inputSerial").removeClass("input_err");
            $('#btnReq').prop('disabled', false);
        } else {
            $("#inputEmail").addClass("input_err");
            $("#inputSerial").addClass("input_err");
            $('#btnReq').prop('disabled', 'disabled');
        }
    });
    var Lang = readCookie("SelectedLanguage");
    switch (Lang) {
        case "10":
            required = "jap";
            break;
        case "2":
            required = "Requis";
            emailMessage = "L'adresse e-mail n'est pas valide";
            serialNoMessage1 = "Le numéro de série se compose de 8 chiffres";
            serialNoMessage2 = "Le numéro de série n'est pas valide";
            break;
        case "3":
            required = "Erforderlich";
            emailMessage = "Die E-Mail-Adresse ist ungültig";
            serialNoMessage1 = "Die Seriennummer muss 8 Ziffern enthalten.";
            serialNoMessage2 = "Die Seriennummer ist ungültig.";
            break;
        case "1":
            required = "Obligatorio";
            emailMessage = "La dirección de correo electrónico no es válida";
            serialNoMessage1 = "El número de serie debe contener 8 dígitos";
            serialNoMessage2 = "El número de serie no es válido";
            break;
        case "4":
            required = "Obbligatorio";
            emailMessage = "Questa email non è valida";
            serialNoMessage1 = "Il numero di serie deve contenere 8 cifre";
            serialNoMessage2 = "Il numero di serie non è valido";
            break;
        case "5":
            required = "필수 항목";
            emailMessage = "유효하지 않은 이메일입니다";
            serialNoMessage1 = "일련 번호는 8자입니다";
            serialNoMessage2 = "유효하지 않은 일련 번호입니다";
            break;
        case "6":
            required = "Obrigatório";
            emailMessage = "Недопустимый адрес электронной почты";
            serialNoMessage1 = "Серийный номер должен содержать 8 цифр";
            serialNoMessage2 = "Недопустимый серийный номер";
            break;
        case "7":
            required = "Обязательно";
            emailMessage = "O e-mail não é válido";
            serialNoMessage1 = "O número de série requer 8 dígitos";
            serialNoMessage2 = "O número de série não é válido";
            break;
        case "8":
            required = "必需";
            emailMessage = "电子邮件地址无效";
            serialNoMessage1 = "序列号必须为 8 位";
            serialNoMessage2 = "序列号无效";
            break;
        case "9":
            required = "必填";
            emailMessage = "電子郵件地址無效";
            serialNoMessage1 = "序號必須為 8 位數";
            serialNoMessage2 = "序號無效";
            break;
        case "0":
            required = "Required";
            emailMessage = "The email is not valid";
            serialNoMessage1 = "The serial number requires 8 digits";
            serialNoMessage2 = "The serial number is not valid";
            break;
        default:
            required = "Required";
            emailMessage = "The email is not valid";
            serialNoMessage1 = "The serial number requires 8 digits";
            serialNoMessage2 = "The serial number is not valid";
    }


    $(".inputEmail").rules("add", {
        required: true,
        email: "email",
        messages: {
            required: required,
            email: emailMessage
        }
    });

    $(".inputSerial").rules("add", {
        required: true,
        number: true,
        minlength: 8,
        maxlength: 8,
        messages: {
            required: required,
            number: serialNoMessage2,
            minlength: serialNoMessage1,
            maxlength: serialNoMessage1
        }
    });

    $('#mediaForm').validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class"
    });

    $('#mediaForm input').on('blur', function () {
        if ($('#mediaForm').valid()) {
            $("#scanner_serial_no").removeClass("input_err");
            $("#contact_name").removeClass("input_err");
            $("#street_address").removeClass("input_err");
            $("#city").removeClass("input_err");
            $("#state_or_region").removeClass("input_err");
            $("#country").removeClass("input_err");
            $("#postal_code").removeClass("input_err");
            $('#btnMed').prop('disabled', false);
        } else {
            $("#scanner_serial_no").addClass("input_err");
            $("#contact_name").addClass("input_err");
            $("#street_address").addClass("input_err");
            $("#city").addClass("input_err");
            $("#state_or_region").addClass("input_err");
            $("#country").addClass("input_err");
            $("#postal_code").addClass("input_err");
            $('#btnMed').prop('disabled', 'disabled');
        }
    });

    $(".scanner_serial_no").rules("add", {
        required: true,
        number: true,
        minlength: 8,
        maxlength: 8,
        messages: {
            required: required,
            number: serialNoMessage2,
            minlength: serialNoMessage1,
            maxlength: serialNoMessage1
        }
    });

    $(".contact_name").rules("add", {
        required: true,
        messages: {
            required: required
        }
    });

    $(".street_address").rules("add", {
        required: true,
        messages: {
            required: required
        }
    });

    $(".city").rules("add", {
        required: true,
        messages: {
            required: required
        }
    });

    $(".state_or_region").rules("add", {
        required: true,
        messages: {
            required: required
        }
    });

    $(".country").rules("add", {
        required: true,
        messages: {
            required: required
        }
    });

    $(".postal_code").rules("add", {
        required: true,
        messages: {
            required: required
        }
    });

    $("#email_id").keyup(function () {
        $(".email_id").rules("add", {
            email: true
        });
    });

    $(".close").click(function () {
        var closeMessage = window.location.protocol + "//" + window.document.domain + "/" + window.location.pathname;
        close_path = closeMessage.replace(/([^:]\/)\/+/g, "$1");
        window.top.location = close_path;
    });

    $(".close_msg").click(function () {
        $.ajax({
            url: 'index.php',
            type: 'post',
            data: 'sure=1', //send a value to make sure we want to destroy it.
            success: function (data) {
                //alert(data);
            }
        });
    });

    $.validator.addMethod("email", function (value, element)
    {
        return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
    }, emailMessage);

    $("#inputSerial").on("input", function () {
        if ($('#inputSerial').val().length > "7" && $('#inputSerial').val().length < "9") {
            checkSerialNumber();
        } else {
            $("#product-status").removeClass("fa-check fa-close");
            $('.validModelNo').hide();
            $('.validModelNo span').text('');
        }
    });

    $('#submit').val('Choose items above');
    $("#submit").attr('disabled', 'disabled');

    var $checkboxes = $('#downloadForm input[type="checkbox"]');

    $checkboxes.change(function () {
        var submit = $checkboxes.filter(':checked').length;
        if ($(submit).length > 0)
        {
            $('#submit').val("Download " + submit + " items");
            $("#submit").attr("disabled", false);
        } else
        {
            $('#submit').val('Choose items above');
            $("#submit").attr('disabled', 'disabled');
        }
    });

    $("#indexForm").submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            data: $("#indexForm").serialize(),
            url: 'serialNumberVerify.php',
            beforeSend: function () {
                $('.loader').show()
            },
            complete: function () {
                $('.loader').hide();
            },
            success: function (data) {
                var pattern = /true/;
                var exists = pattern.test(data);
                if (exists) {
                    $('.successCont').show();
                    $('.errorCont').hide();
                } else {
                    $('.successCont').hide();
                    $('.errorCont').show();
                }
                $('.modalMailId').text($('#inputEmail').val());
                $("#confirmRequest").modal("show");
            }
        });
        return false;
    });
});

function checkSerialNumber() {
    $('#inputSerial').addClass('inputLoader').prop('disabled', true);
    $("#product-status").removeClass("glyphicon-ok glyphicon-remove");
    var serialNo = $("#inputSerial").val();

    $.ajax({
        method: "POST",
        url: "index.php",
        data: "serialNo=" + serialNo,
        success: function (data) {
            var product_id = readCookie('product_id');
            var model_number = readCookie('model_number');
            if (product_id == "" || model_number == "") {
                $("#product-status").addClass('invalid glyphicon-remove').removeClass('valid glyphicon-ok');
                $('.validModelNo').hide();
                $('.validModelNo span').text('');
            } else {
                delete_cookie('product_id');
                $("#product-status").addClass('valid glyphicon-ok').removeClass('invalid glyphicon-remove');
                $('.validModelNo').show();
                $('.validModelNo span').text(model_number);
                if ($('#inputEmail').hasClass('my-valid-class')) {
                    $('#btnReq').prop('disabled', false);
                }
            }
            $('#inputSerial').removeClass('inputLoader').prop('disabled', false);
        },
        error: function (response) {
//            alert(response);
        }
    });
}

function setCookie(cookieName, cookieValue, nDays) {
    var today = new Date();
    var expire = new Date();

    if (!nDays)
        nDays = 1;

    expire.setTime(today.getTime() + 3600000 * 24 * nDays);
    //document.cookie = cookieName + "=" + escape(cookieValue) + ";expires=" + expire.toGMTString();
    document.cookie = cookieName + "=" + escape(cookieValue) + ";expires=" + expire.toGMTString() + ";secure";
}
function readCookie(cookieName) {
    var re = new RegExp('[; ]' + cookieName + '=([^\\s;]*)');
    var sMatch = (' ' + document.cookie).match(re);
    return (cookieName && sMatch) ? unescape(sMatch[1]) : '';
}
function delete_cookie(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

$(window).load(function () {
    if (readCookie('SelectedLanguage') != '') {
        $("#lang_dd").prop('selectedIndex', readCookie('SelectedLanguage'));
        $("#lang_dd").bind("DOMSubtreeModified", function () {
            $("#lang_dd").prop('selectedIndex', readCookie('SelectedLanguage'));
        });
    }

    $('#lang_dd').on('change', function () {
        setCookie("SelectedLanguage", $("#lang_dd").prop('selectedIndex'));
        $("#lang_dd").prop('selectedIndex', readCookie('SelectedLanguage'));
//            if (readCookie('SelectedLanguage') != "") {
//                window.location.search = this.value;
//            }
        if (window.location.search != "") {
            if (window.location.search.indexOf('lang') > -1) {
                window.location.search = window.location.search.replace(getParameterByName('lang'), this.value.split('=')[1]);
            } else {
                window.location.search = window.location.search + "&" + this.value.replace('?', '');
            }
        } else {
            window.location.search = this.value;
        }
    });

});

function wheretoplace() {
    var width = window.innerWidth;
    if (width < 500)
        return "top";
    return "right";
}
