<?php

include("config.php");

$prodFamily = trim($_POST['productS']);
$prodName = trim($_POST['friendlyN']);
$serialRange = trim($_POST['serialN']);
$copiedProdName = trim($_POST['copiedProdName']);
$copiedProdFName = trim($_POST['copiedProdFName']);

require_once 'WindowsAzure/WindowsAzure.php';
require_once 'HTTP/Request2.php';

use WindowsAzure\Blob\Models\BlobBlockType;
use WindowsAzure\Blob\Models\Block;
use WindowsAzure\Blob\Models\CommitBlobBlocksOptions;
use WindowsAzure\Blob\Models\CreateBlobOptions;
use WindowsAzure\Blob\Models\CreateContainerOptions;
use WindowsAzure\Blob\Models\ListContainersOptions;
use WindowsAzure\Blob\Models\PublicAccessType;
use WindowsAzure\Common\Internal\IServiceFilter;
use windowsazure\common\Internal\Resources;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Common\CloudConfigurationManager;

class AzureAPI {

    function __construct($connectionString) {
        $this->blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);
    }

    function renameBlob($source_container, $source_name, $destination_container, $destination_name) {
        try {
            $this->blobRestProxy->copyBlob($destination_container, $destination_name, $source_container, $source_name);
            $this->blobRestProxy->deleteBlob($source_container, $source_name);
        } catch (ServiceException $e) {
            die($e->getMessage());
        }
        return true;
    }

}

$connectionString = 'DefaultEndpointsProtocol=https;AccountName=iminteractiveinstall2199;AccountKey=77uYvNagWxCt5V2JTsk+FCAoADdx+O1NoNrHLPXdX8LQjr9mmj+8tiAQ33bONA304QcRT8OEtZun1oaqC0BEYw==';

$azure = new AzureAPI($connectionString);
$blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);
$blob_list = $blobRestProxy->listBlobs($copiedProdFName);
$blobs = $blob_list->getBlobs();
try {
    $blobRestProxy->getContainerProperties($prodFamily);
    // The container exists.
    echo "<br/>The container exists.";
//    foreach ($blobs as $key => $blob) {
//        $azure->renameBlob($copiedProdFName, $blob->getName(), $prodFamily, $blob->getName());
//    }
} catch (ServiceException $e) {
    // Code ContainerNotFound (404) means the container does not exist.
    $code = $e->getCode();
    echo "<br/>The does not container exists.";
    $createContainerOptions = new CreateContainerOptions();
    $createContainerOptions->setPublicAccess(PublicAccessType::CONTAINER_AND_BLOBS);
    $blobRestProxy->createContainer($prodFamily, $createContainerOptions);
    foreach ($blobs as $key => $blob) {
        $azure->renameBlob($copiedProdFName, $blob->getName(), $prodFamily, $blob->getName());
    }
}


//-----------------db insertion -----------------


$insert_prod_owner_list = "INSERT INTO [ciotiswd].product_owner_list(product_family, product_name, serial_range) VALUES ('$prodFamily', '$prodName', '$serialRange') ";
echo $insert_prod_owner_list;
$insertProdOwnerList = sqlsrv_query($conn, $insert_prod_owner_list);

$insert_prod_owner_list_content = "IF NOT EXISTS ( SELECT product_family, friendly_name FROM [ciotiswd].product_owner_list_content WHERE product_owner_list_id = (SELECT id FROM [ciotiswd].product_owner_list WHERE product_name = '$prodName') ) BEGIN 
INSERT INTO [ciotiswd].product_owner_list_content(product_owner_list_id, product_family, category, friendly_name, version, description, filename, os, language)
SELECT
  (SELECT id FROM [ciotiswd].product_owner_list WHERE product_name = '$prodName'), (SELECT product_family FROM [ciotiswd].product_owner_list WHERE id = (SELECT id FROM [ciotiswd].product_owner_list WHERE product_name = '$prodName')), category, friendly_name, version, description, filename, os, language
FROM
  [ciotiswd].product_owner_list_content
WHERE
  product_owner_list_id = (SELECT id FROM [ciotiswd].product_owner_list WHERE product_name = '$copiedProdName')
END";
echo $insert_prod_owner_list_content;
$insertProdOwnerListContent = sqlsrv_query($conn, $insert_prod_owner_list_content);
?>