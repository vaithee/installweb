<?php

class Contact extends Microsoft_WindowsAzure_Storage_TableEntity {

    /**
     * @azure Name
     */
    public $Name;

    /**
     * @azure Address
     */
    public $Address;

    /**
     * @azure Phone
     */
    public $Phone;

}

?>