<?php

$query = "BACKUP DATABASE ka-msftsql TO DISK = 'ka-msftsql.bak'";

$conn = sqlsrv_connect('kodakalaris.database.windows.net', array('UID' => 'dbadmin', 'PWD' => 'Abcd123$', 'Database' => 'ka-msftsql'));

if (!$conn) {
    print_r(sqlsrv_errors());
    exit;
}

sqlsrv_configure("WarningsReturnAsErrors", 0);
if (($stmt = sqlsrv_query($conn, $query))) {
    do {
        /* print_r(sqlsrv_errors());
          echo " * ---End of result --- *\r\n"; */
    } while (sqlsrv_next_result($stmt));
    sqlsrv_free_stmt($stmt);
}
sqlsrv_configure("WarningsReturnAsErrors", 1);
sqlsrv_close($conn);
?>