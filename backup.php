<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require 'ch_debug.php';
require 'config_cron.php';
$log_backup_file = "D:\home\site\wwwroot\last-test-log.bak";

$sql = "BACKUP DATABASE ka-msftsql TO DISK = '$backup_file' ";

ch_debug($sql);

$stmt = sqlsrv_query($conn, $sql);

if ($stmt === false)
    ch_debug(sqlsrv_errors());
else
    ch_debug("Database backed up to $backup_file <br>");

//Backup log. Put DB into "Restoring..." state.
$sql = "BACKUP LOG ka-msftsqlDB TO DISK = '$log_backup_file' WITH NORECOVERY";
ch_debug($sql);
$stmt = sqlsrv_query($conn, $sql);
if ($stmt === false)
    ch_debug(print_r(sqlsrv_errors()));
else
    ch_debug("Transaction log backed up to $log_backup_file");
