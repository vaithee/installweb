<?php
session_start();

$BASE_PATH = parse_url(strtok("https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", '?'));
$BASE_PATH = "https://" . $BASE_PATH["host"];
$IMAGE_PATH = $BASE_PATH . "/images/";

header('Cache-control: private'); // IE 6 FIX

if (isSet($_POST['lang'])) {
    $lang = $_POST['lang'];
    $_SESSION['lang'] = $lang;
    //setcookie('lang', $lang, time() + (3600 * 24 * 30));
    setcookie('lang', $lang, time() + (3600 * 24 * 30), '/', $_SERVER['SERVER_NAME'], true, false);
} else if (isSet($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else if (isSet($_COOKIE['lang'])) {
    $lang = $_COOKIE['lang'];
} else {
    $lang = '?lang=en-US';
}

switch ($lang) {
    case '?lang=en-US':
        $lang_file = 'en/lang.en.php';
        break;
    case '?lang=fr':
        $lang_file = 'fr/lang.fr.php';
        break;
    case '?lang=ja':
        $lang_file = 'ja/lang.ja.php';
        break;
    case '?lang=de':
        $lang_file = 'de/lang.de.php';
        break;
    case '?lang=es':
        $lang_file = 'es/lang.es.php';
        break;
    case '?lang=it':
        $lang_file = 'it/lang.it.php';
        break;
    case '?lang=ko':
        $lang_file = 'ko/lang.ko.php';
        break;
    case '?lang=pt-br':
        $lang_file = 'pt-br/lang.pt-br.php';
        break;
    case '?lang=ru':
        $lang_file = 'ru/lang.ru.php';
        break;
    case '?lang=zh-cn':
        $lang_file = 'zh-cn/lang.zh-cn.php';
        break;
    case '?lang=zh-tw':
        $lang_file = 'zh-tw/lang.zh-tw.php';
        break;
    default:
        $lang_file = 'en/lang.en.php';
}

include_once 'languages/' . $lang_file;

//$available_langs = array('en', 'fr', 'ja');
//$_SESSION['lang'] = 'en';
//if (isset($_GET['lang']) && $_GET['lang'] != '') {
//    if (in_array($_GET['lang'], $available_langs)) {
//        $_SESSION['lang'] = $_GET['lang'];
//    }
//}
//include('languages/' . $_SESSION['lang'] . '/lang.' . $_SESSION['lang'] . '.php');
//unset($_COOKIE["langUrl"]);
//if (isset($_COOKIE["langUrl"])) {
//    echo $_COOKIE["langUrl"];
//    header("location:http://kainstall.azurewebsites.net/".$_COOKIE["langUrl"]);
//    unset($_COOKIE["langUrl"]);
//} else {
//    header("http://kainstall.azurewebsites.net/");
//}
?>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header col-xs-12 col-sm-12 col-md-12 padLR0">
            <a class="navbar-brand col-xs-6 col-sm-6 col-md-6" href="<?php echo $BASE_PATH; ?>"><img src="<?php echo $IMAGE_PATH; ?>KodakAlarisweblogo.png" class="img-responsive" style="display:inline;"><span class="info hidden-xs">Information Management</span></a>
            <div class="select_div col-xs-6 col-sm-6 col-md-6 text-right">
                <select id="lang_dd" class="btn btn-sm btn-default">
                </select>
            </div>
        </div>
    </div>
</nav>