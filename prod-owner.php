<!DOCTYPE html>
<head>
    <title>Kodak Alaris::Information Management</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/ico" href="images/favicon.ico">
    <link rel="icon" type="image/x-icon" href="images/favicon.ico">
    <link rel="icon" type="image/png" href="images/favicon.ico">
    <link href="css/fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="css/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link href="css/prod-owner.css" rel="stylesheet" type="text/css">
    <script src="js/jquery-2.2.3.min.js" type="text/javascript"></script>
    <script src="js/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.validate.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
</head>
<body>
    <?php
    set_time_limit(0);
    include("config.php");
//    require_once('/simplesamlphp/lib/_autoload.php');
//    $as = new SimpleSAML_Auth_Simple('default-sp');
//    $as->requireAuth();
//    $attributes = $as->getAttributes();

    require_once 'WindowsAzure/WindowsAzure.php';
    require_once 'HTTP/Request2.php';

    use WindowsAzure\Blob\Models\BlobBlockType;
    use WindowsAzure\Blob\Models\Block;
    use WindowsAzure\Blob\Models\CommitBlobBlocksOptions;
    use WindowsAzure\Blob\Models\CreateBlobOptions;
    use WindowsAzure\Blob\Models\CreateContainerOptions;
    use WindowsAzure\Blob\Models\ListContainersOptions;
    use WindowsAzure\Blob\Models\PublicAccessType;
    use WindowsAzure\Common\Internal\IServiceFilter;
    use windowsazure\common\Internal\Resources;
    use WindowsAzure\Common\ServiceException;
    use WindowsAzure\Common\ServicesBuilder;

$getAllProducts = "SELECT * FROM [ciotiswd].product_owner_list";
    $stmt = sqlsrv_query($conn, $getAllProducts);
    $productList = array();
    while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
        $productList[] = $row;
    }
    if (isset($_POST['updateFreindlyNameVal'])) {
        $currentPF = $_POST['pf'];
        $updateFreindlyNameVal = $_POST['updateFreindlyNameVal'];
        $currentFileName = $_POST['currentFileName'];
        $changedOS = $_POST['changedOS'];
        $changedLang = $_POST['changedLang'];
        $update_prod_owner_list_content = "UPDATE [ciotiswd].product_owner_list_content SET friendly_name = '$updateFreindlyNameVal', os='$changedOS', language='$changedLang' WHERE filename = '$currentFileName' and product_family = '$currentPF'";
        $updateProdOwnerListContent = sqlsrv_query($conn, $update_prod_owner_list_content);
    }
    if (isset($_POST['deleteRowFromProdListContent'])) {
        $deleteRowFromProdListContent = $_POST['deleteRowFromProdListContent'];
        $deleteRowProdSeries = $_POST['deleteRowProdSeries'];
        $deleteRowProdName = $_POST['deleteRowProdName'];
        $delete_prod_list = "DELETE FROM [ciotiswd].product_owner_list_content WHERE filename='$deleteRowFromProdListContent' and product_family='$deleteRowProdSeries' and product_owner_list_id=(SELECT id FROM [ciotiswd].product_owner_list WHERE product_name = '$deleteRowProdName' and product_family = '$deleteRowProdSeries') ";
        $deleteProdList = sqlsrv_query($conn, $delete_prod_list);
    }
    if (isset($_POST['deleteContainer'])) {
        $containerToDelete = $_POST['deleteContainer'];
        $delete_prod_list_content = "DELETE FROM [ciotiswd].product_owner_list_content WHERE product_owner_list_id =(SELECT id FROM [ciotiswd].product_owner_list WHERE product_name = '$containerToDelete')";
        $deleteProdListContent = sqlsrv_query($conn, $delete_prod_list_content);
        $delete_prod_list = "DELETE FROM [ciotiswd].product_owner_list WHERE product_name='$containerToDelete'";
        $deleteProdList = sqlsrv_query($conn, $delete_prod_list);
    }
    if (isset($_POST['updateProdDetailsTable'])) {
        $prodFamily = $_POST['updatedProdFamily'];
        $prodName = $_POST['updatedProdName'];
        $serialNo = $_POST['updatedSerialNo'];
        $range = explode('-', $serialNo);
        $startNo = $range[0];
        $endNo = $range[1];
//        echo $update_prod_details_tbl = "declare @id int select @id = '$startNo' while @id >='$startNo' and @id <= '$endNo' begin INSERT INTO [ciotiswd].[dummy_product_details] (product_category, serial_no, product_family, model_number) VALUES ('Scanner', @id, '$prodFamily', '$prodName') select @id = @id + 1 end GO";
        echo $update_prod_details_tbl = "declare @id int"
        . " select @id = '$startNo'"
        . " while @id >='$startNo' and @id <= '$endNo'"
        . " begin SET NOCOUNT ON"
        . " INSERT INTO [ciotiswd].[dummy_product_details] (product_category, serial_no, product_family, model_number) VALUES ('Scanner', @id, '$prodFamily', '$prodName')"
        . " select @id = @id + 1"
        . " end";
        $updateProdDetailsTbl = sqlsrv_query($conn, $update_prod_details_tbl);
        $insert_distinct_prod_details_tbl = "IF NOT EXISTS ( SELECT product_family FROM [ciotiswd].distinct_product_details WHERE model_number = '$prodName' ) BEGIN INSERT INTO [ciotiswd].distinct_product_details(product_family, model_number, serial_no) VALUES ('$prodFamily', '$prodName', '$serialNo') END";
        $insertDistinctProdDetailsTbl = sqlsrv_query($conn, $insert_distinct_prod_details_tbl);
    }
    if (isset($_POST['saveProductFamily'])) {
        $prodFamily = $_POST['updatedProdFamily'];
        $prodName = $_POST['updatedProdName'];
        $serialNo = $_POST['updatedSerialNo'];
        $insert_prod_owner_list = "IF NOT EXISTS ( SELECT product_family, product_name FROM [ciotiswd].product_owner_list WHERE serial_range = '$serialNo' ) BEGIN INSERT INTO [ciotiswd].product_owner_list(product_family, product_name, serial_range) VALUES ('$prodFamily', '$prodName', '$serialNo') END";
        echo $insert_prod_owner_list;
        $insertProdOwnerList = sqlsrv_query($conn, $insert_prod_owner_list);
    }
    ?>
    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><span>product software management console</span></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                        <a href="prod-owner.php">Products</a>
                    </li>
                    <li>
                        <a href="reports.php">Reports</a>
                    </li>
                    <!--                    <li>
                                            <a href="module.php/core/authenticate.php?as=default-sp&logout"><i class="fa fa-power-off" aria-hidden="true" style="font-size: 20px;"></i></a>
                                        </li>-->
                </ul>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Products</h1>
                    <p>Manage products and associated software downloads.</p>
                    <a class="btn btn-link addProdLink" data-toggle="modal" data-target="#product"><i class="fa fa-fw fa-plus-circle"></i> Add product</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2">Product Family</div>
                                <div class="col-md-3">Product</div>
                                <div class="col-md-3">Serial # range(s)</div>
                                <div class="col-md-2">Downloads</div>
                                <div class="col-md-2">Tools</div>
                            </div>
                        </div>
                        <ul class="list-group" id="prodList">
                            <?php
                            if (!empty($productList)) {
                                foreach ($productList as $key => $list) {
                                    $productId = $list['id'];
                                    $get_product_list_content = "SELECT * FROM [ciotiswd].product_owner_list_content where product_owner_list_id= '$productId'";
                                    $getProductListContent = sqlsrv_query($conn, $get_product_list_content);
                                    $productListContent = array();
                                    while ($row = sqlsrv_fetch_array($getProductListContent, SQLSRV_FETCH_ASSOC)) {
                                        $productListContent[] = $row;
                                    }
                                    ?>
                                    <li class="list-group-item">
                                        <div class="row collapsed" role="button" data-toggle="collapse" href="#sw<?php echo $key ?>" aria-expanded="false" aria-controls="sw1">
                                            <div class="col-md-2 clonedProdFName"><?php echo $list['product_family'] ?></div>
                                            <div class="col-md-3 clonedProdName"><?php echo $list['product_name'] ?></div>
                                            <div class="col-md-3 clonedSerialRange"><?php echo $list['serial_range'] ?></div>
                                            <div class="col-md-2">
                                                <span class="badge">0</span>&nbsp;downloads
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <a href="#" class="editLi"><i class="fa fa-fw fa-pencil pull-left" data-toggle="tooltip" data-placement="top" title="edit"></i></a>
                                                <a href="#" class="copyLi"><i class="fa fa-fw fa-copy" data-toggle="tooltip" data-placement="top" title="copy"></i></a>
                                                <a href="#" class="deleteLi" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-fw fa-trash pull-right" data-toggle="tooltip" data-placement="top" title="delete"></i></a>
                                            </div>
                                        </div>
                                        <div class="collapse" id="sw<?php echo $key ?>">
                                            <div class="well">
                                                <div class=" table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Friendly Name</th>
                                                                <th>Filename</th>
                                                                <th>OS</th>
                                                                <th>Language</th>
                                                                <th>Tools</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if (!empty($productListContent)) {
                                                                foreach ($productListContent as $key => $content) {
                                                                    ?>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td class="friendlyNameTd"><?php echo $content['friendly_name'] ?></td>
                                                                        <td class="fileNameTd"><?php echo $content['filename'] ?></td>
                                                                        <td class="osTd"><?php echo $content['os'] ?></td>
                                                                        <td class="langNameTd"><?php echo $content['language'] ?></td>
                                                                        <td class="text-center iconTd">
                                                                            <a class="infoIcon" href="#" data-trigger="hover" title="info" data-content="<?php echo $content['description'] ?>" data-placement="top" data-toggle="popover"><i class="fa fa-fw fa-info-circle pull-left"></i></a>
                                                                            <a href="#" class="editProdContent"><i class="fa fa-fw fa-pencil" data-toggle="tooltip" data-placement="top" title="edit"></i></a>
                                                                            <a href="#" class="deleteProdContent" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-fw fa-minus-circle pull-right" data-toggle="tooltip" data-placement="top" title="remove"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="btn-group">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Add file... <span class="fa fa-caret-down"></span></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li>
                                                            <a class="addFileDD" href="#" data-toggle="modal" data-target="#download">New driver...</a>
                                                        </li>
                                                        <li>
                                                            <a class="addFileDD"  href="#" data-toggle="modal" data-target="#download">New application...</a>
                                                        </li>
                                                        <li>
                                                            <a class="addFileDD"  href="#" data-toggle="modal" data-target="#download">New documentation...</a>
                                                        </li>
                                                        <?php
                                                        $connectionString = 'DefaultEndpointsProtocol=https;AccountName=iminteractiveinstall2199;AccountKey=77uYvNagWxCt5V2JTsk+FCAoADdx+O1NoNrHLPXdX8LQjr9mmj+8tiAQ33bONA304QcRT8OEtZun1oaqC0BEYw==';
                                                        $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);
                                                        try {
                                                            $blob_list = $blobRestProxy->listBlobs(strtolower(str_replace(" ", "-", trim($list['product_family']))));
                                                            $blobs = $blob_list->getBlobs();
                                                        } catch (ServiceException $e) {
                                                            $code = $e->getCode();
                                                            $error_message = $e->getMessage();
                                                            $code . ": " . $error_message . "<br />";
                                                        }
                                                        if (isset($blobs)) {
                                                            foreach ($blobs as $key => $blob) {
                                                                ?>
                                                                <li>
                                                                    <a class="addFileDD"  href="#" data-toggle="modal" data-target="#addExistingProdModal"><?php echo $blob->getName() ?></a>
                                                                </li>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                                <!--<div class="btn btn-primary pull-right disabled">Publish</div>-->
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }
                            } else {
                                ?>
                                <li class="list-group-item text-center noIemsMsg">There are no items in the list</li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-link addProdLink" data-toggle="modal" data-target="#product"><i class="fa fa-fw fa-plus-circle"></i> Add product</a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="product">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Product&nbsp;</h4>
                </div>
                <form role="form" id="copyProdForm" method="post">
                    <div class="modal-body">
                        <div class="form-group pfBlk">
                            <label class="control-label" for="productS">Existing product family</label>
                            <a class="btn btn-link pull-right addNewFamilyLink"><i class="fa fa-fw fa-plus-circle"></i> or add new family</a>
                            <a class="btn btn-link pull-right cancelFamilyLink"><i class="fa fa-fw fa-times-circle"></i> cancel</a>
                            <input class="form-control inputEditProductS" name="inputEditProductS" id="inputEditProductS" placeholder="i900-series" type="text">
                            <select class="form-control productS" id="productS"  name="productS">
                                <?php
                                if (isset($_POST['getDistinctProdFamilies'])) {
                                    $getDistinctProdFamilies = "SELECT DISTINCT [ciotiswd].distinct_product_details.product_family FROM [ciotiswd].distinct_product_details";
                                    $get_distinct_prod_families = sqlsrv_query($conn, $getDistinctProdFamilies);
                                    while ($row = sqlsrv_fetch_array($get_distinct_prod_families)) {
                                        echo "<option value='$row[0]'>" . preg_replace("/[\s_]/", "-", strtolower($row[0])) . "</option>";
                                    }
                                }
                                ?>
                            </select>
                            <p class="help-block">Container names must begin with a letter or a number and also between 3 and 63 characters long.</p>
                        </div>
                        <div class="form-group pnBlk">
                            <label class="control-label" for="friendlyN">Existing product name</label>
                            <a class="btn btn-link pull-right addNewProdLink"><i class="fa fa-fw fa-plus-circle"></i> or add new product</a>
                            <a class="btn btn-link pull-right cancelProdLink"><i class="fa fa-fw fa-times-circle"></i> cancel</a>
                            <input class="form-control inputEditFriendlyN" name="inputEditFriendlyN" id="inputEditFriendlyN" placeholder="Scan Mate i000 Scanner" type="text">
                            <select class="form-control friendlyN" id="friendlyN"  name="friendlyN">
                                <?php
                                if (isset($_POST['getDistinctProdName'])) {
                                    $changedPN = $_POST['getDistinctProdName'];
                                    $getDistinctProdName = "SELECT DISTINCT model_number FROM [ciotiswd].distinct_product_details where product_family = '$changedPN'";
                                    $get_distinct_prod_name = sqlsrv_query($conn, $getDistinctProdName);
                                    while ($row = sqlsrv_fetch_array($get_distinct_prod_name)) {
                                        echo "<option value='$row[0]'>$row[0]</option>";
                                    }
                                }
                                ?>
                            </select>
                            <p class="help-block">This is the consumer-facing product name</p>
                        </div>
                        <div class="form-group snBlk">
                            <label class="control-label" for="serialN">Serial number range(s)</label>
                            <?php
                            if (isset($_POST['getDistinctSno'])) {
                                $changedPF = $_POST['getDistinctProdFamily'];
                                $changedPN = $_POST['getDistinctProdName'];
                                $getDistinctSno = "SELECT DISTINCT serial_no FROM [ciotiswd].distinct_product_details where product_family = '$changedPF' and model_number = '$changedPN'";
                                $get_distinct_s_no = sqlsrv_query($conn, $getDistinctSno);
                                while ($row = sqlsrv_fetch_array($get_distinct_s_no)) {
                                    $sNo = $row[0];
                                    echo '<input class="form-control serialN" name="serialN" id="serialN" type="text" readonly="true" value="' . $sNo . '" />';
                                }
                            }
                            ?>
                            <input class="form-control serialN" name="serialN" id="serialN" type="text" readonly="true" value="" />
                            <p class="help-block">Separate start and end with "-". Separate multiple ranges with ","</p>
                        </div>
                        <p class="text-right">Downloads are added after saving.</p>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default" data-dismiss="modal">Cancel</a>
                        <button type="button" class="btn btn-primary" id="addProdBtn">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="download">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Download</h4>
                </div>
                <form role="form" id="downloadModal" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label" for="cat">Category</label>
                            <select class="form-control modal_catDd" id="modal_catDd"  name="modal_catDd">
                                <option>Driver</option>
                                <option>Application</option>
                                <option>Documentation</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="modal_Friendlyname">Friendly name</label>
                            <input class="form-control modal_Friendlyname" name="modal_Friendlyname" id="modal_Friendlyname" placeholder="Some software" type="text">
                            <p class="help-block">This is the consumer-facing package name</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="modal_versionNumber">Version number</label>
                            <input class="form-control modal_versionNumber" name="modal_versionNumber" id="modal_versionNumber" placeholder="1.0.0" type="text">
                        </div>
                        <div class="form-group">
                            <label for="modal_inf">Description</label>
                            <textarea class="form-control modal_inf" rows="3" name="modal_inf" id="modal_inf"></textarea>
                            <p class="help-block">This is the consumer-facing description</p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">File upload</label>
                            <input class="exampleInputFile" name="exampleInputFile" id="exampleInputFile" type="file">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="lang">Operating system:&nbsp;</label>
                            <label class="radio-inline">
                                <input type="radio" name="inlineRadioOptions" class="inlineRadio0" id="inlineRadio0" value="option0" checked="">
                                Any
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="inlineRadioOptions" class="inlineRadio1" id="inlineRadio1" value="option1">
                                Windows
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="inlineRadioOptions" class="inlineRadio2"  id="inlineRadio2" value="option2">
                                Mac OS
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="inlineRadioOptions" class="inlineRadio3" id="inlineRadio3" value="option3">
                                Linux
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="modal_lang">Language</label>
                            <select class="form-control modal_lang" name="modal_lang" id="modal_lang">
                                <option>Not language specific</option>
                                <option>English</option>
                                <option>Spanish</option>
                                <option>French</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default" data-dismiss="modal">Cancel</a>
                        <button type="button" class="btn btn-primary" id="addFileToProdBtn">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addExistingProdModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Download</h4>
                </div>
                <form role="form" id="addExistingProdModalForm" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label" for="addExistingProdModal_catDd">Category</label>
                            <select class="form-control addExistingProdModal_catDd" name="addExistingProdModal_catDd" id="addExistingProdModal_catDd">
                                <option>Driver</option>
                                <option>Application</option>
                                <option>Documentation</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="addExistingProdModal_Friendlyname">Friendly name</label>
                            <input class="form-control addExistingProdModal_Friendlyname" name="addExistingProdModal_Friendlyname" id="addExistingProdModal_Friendlyname" placeholder="Some software" type="text">
                            <p class="help-block">This is the consumer-facing package name</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="addExistingProdModal_Filename">File name</label>
                            <input class="form-control addExistingProdModal_Filename" name="addExistingProdModal_Filename" id="addExistingProdModal_Filename" type="text" readonly>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="addExistingProdModal_versionNumber">Version number</label>
                            <input class="form-control addExistingProdModal_versionNumber" name="addExistingProdModal_versionNumber" id="addExistingProdModal_versionNumber" placeholder="1.0.0" type="text">
                        </div>
                        <div class="form-group">
                            <label for="addExistingProdModal_inf">Description</label>
                            <textarea class="form-control addExistingProdModal_inf" rows="3" name="addExistingProdModal_inf" id="addExistingProdModal_inf"></textarea>
                            <p class="help-block">This is the consumer-facing description</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="lang">Operating system:&nbsp;</label>
                            <label class="radio-inline">
                                <input type="radio" name="addExistingProdModalRadioOptions" class="addExistingProdModalRadio0" id="addExistingProdModalRadio0" value="option0" checked="">
                                Any
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="addExistingProdModalRadioOptions" class="addExistingProdModalRadio1" id="addExistingProdModalRadio1" value="option1">
                                Windows
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="addExistingProdModalRadioOptions" class="addExistingProdModalRadio2"  id="addExistingProdModalRadio2" value="option2">
                                Mac OS
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="addExistingProdModalRadioOptions" class="addExistingProdModalRadio3" id="addExistingProdModalRadio3" value="option3">
                                Linux
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="addExistingProdModal_lang">Language</label>
                            <select class="form-control addExistingProdModal_lang" name="addExistingProdModal_lang" id="addExistingProdModal_lang">
                                <option>Not language specific</option>
                                <option>English</option>
                                <option>Spanish</option>
                                <option>French</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-default" data-dismiss="modal">Cancel</a>
                        <button type="button" class="btn btn-primary" id="addExistingProdModalSave">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
                </div>

                <div class="modal-body">
                    <p>You are about to delete one record, this procedure is irreversible.</p>
                    <p>Do you want to proceed?</p>
                    <p class="containerNameToDelete"></p>
                    <p class="blobNameToDelete"></p>
                    <p class="deleteRowProdSeries" style="display: none;"></p>
                    <p class="deleteRowProdName" style="display: none;"></p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok deleteRowFromProdListContent">Delete</a>
                </div>
            </div>
        </div>
    </div>

    <!--to clone li-->
<li class="list-group-item" id="clonedLi" style="display: none;">
    <div class="row collapsed" role="button" data-toggle="collapse" aria-expanded="false">
        <div class="col-md-2 clonedProdFName" id="clonedProdFName"></div>
        <div class="col-md-3 clonedProdName" id="clonedProdName"><span class="text-danger">(pending)</span></div>
        <div class="col-md-3 clonedSerialRange" id="clonedSerialRange"></div>
        <div class="col-md-2">
            <span class="badge">0</span>&nbsp;downloads
        </div>
        <div class="col-md-2 text-center">
            <a href="#" class="editLi"><i class="fa fa-fw fa-pencil pull-left" data-toggle="tooltip" data-placement="top" title="edit"></i></a>
            <a href="#" class="copyLi"><i class="fa fa-fw fa-copy" data-toggle="tooltip" data-placement="top" title="copy"></i></a>
            <a href="#" class="deleteLi" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-fw fa-trash pull-right" data-toggle="tooltip" data-placement="top" title="delete"></i></a>
        </div>
    </div>
    <div class="collapse">
        <div class="well">
            <div class=" table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Friendly Name</th>
                            <th>Filename</th>
                            <th>OS</th>
                            <th>Language</th>
                            <th>Tools</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!--to clone table row-->
                        <tr id="clonedTbl" class="displayNone">
                            <td class="count"></td>
                            <td class="friendlyNameTd friendlyName"></td>
                            <td class="fileNameTd fileName"></td>
                            <td class="osTd os"></td>
                            <td class="langNameTd lang"></td>
                            <td class="iconTd text-center">
                                <a class="displayNone infoIcon" href="#" data-trigger="hover" title="info" data-content="Here would be some information about this download to help me decide if it's right for me." data-placement="top" data-toggle="popover"><i class="fa fa-fw fa-info-circle pull-left"></i></a>
                                <a class="displayNone editProdContent" href="#"><i class="fa fa-fw fa-pencil" data-toggle="tooltip" data-placement="top" title="edit"></i></a>
                                <a class="displayNone deleteProdContent" href="#" data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-fw fa-minus-circle pull-right" data-toggle="tooltip" data-placement="top" title="remove"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="btn-group">
                <a class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Add file... <span class="fa fa-caret-down"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a class="addFileDD"  href="#" data-toggle="modal" data-target="#download">New driver...</a>
                    </li>
                    <li>
                        <a class="addFileDD"  href="#" data-toggle="modal" data-target="#download">New application...</a>
                    </li>
                    <li>
                        <a class="addFileDD"  href="#" data-toggle="modal" data-target="#download">New documentation...</a>
                    </li>

                </ul>
            </div>
            <!--<div class="btn btn-primary pull-right" id="publish">Publish</div>-->
        </div>
    </div>
</li>
<div class="loader"></div>
<div class="updateProdDetailsLoader">
    <div class="loading"></div>
    <div class="loadingContent">
        <div>
            <p>The product data is being added to the database. </p>
            <p>Do not close this browser window until it is complete. This may take several minutes. </p>
        </div>
    </div>
</div>
<script type="text/javascript">
    var addFileRoot;
    var selectedFiles = [];
    var filesToBlob = [];
    var deleteContainerFlag;
    function getDistinctProdFamilies() {
        $.ajax({
            type: "POST",
            url: "prod-owner.php",
            data: {getDistinctProdFamilies: 'true'},
            success: function (data) {
                $('.pfBlk').html($(data).find('.pfBlk').html());
                getDistinctProdNames();
            }
        });
    }
    function getDistinctProdNames() {
        $.ajax({
            type: "POST",
            url: "prod-owner.php",
            beforeSend: function () {
                $('.loader').show();
            },
            data: {getDistinctProdName: $('.productS').val()},
            success: function (data) {
                $('.pnBlk').html($(data).find('.pnBlk').html());
                getDistinctSno();
            }
        });
    }
    function getDistinctSno() {
        $.ajax({
            type: "POST",
            url: "prod-owner.php",
            data: {getDistinctSno: 'true', getDistinctProdFamily: $('.productS').val(), getDistinctProdName: $('.friendlyN').val()},
            beforeSend: function () {
                $('.loader').show();
            },
            success: function (data) {
                $('.serialN').val($(data).find('.serialN').val());
                $('.loader').hide();
            }
        });
    }
    $(document).ready(function () {
        getDistinctProdFamilies();

        $(function () {
            $('[data-toggle="popover"]').popover();
        });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
        $(".badge").each(function () {
            $(this).html($(this).parent().parent().next().find('table>tbody>tr').length);
        });
        $('#addProdBtn').on('click', function () {
            if ($(this).text() == 'Copy') {
                var data = $('#copyProdForm').serialize();
                data += "&copiedProdName=" + encodeURIComponent(copy_pName);
                data += "&copiedProdFName=" + encodeURIComponent(copy_pFName);
//                $.post('copyProduct.php', data);
//                $('#product').modal('toggle');
                $.ajax({
                    type: "POST",
                    url: "copyProduct.php",
                    data: data,
                    beforeSend: function () {
                        $('.loader').show();
                    },
                    complete: function () {
                        $('.loader').hide();
                    },
                    success: function (data) {
                        location.reload();
                    }
                });
            } else if ($(this).text() == 'Update') {
                var data = $('#copyProdForm').serialize();
                data += "&copiedProdName=" + encodeURIComponent(copy_pName);
                data += "&copiedProdFName=" + encodeURIComponent(copy_pFName);
                $.ajax({
                    type: "POST",
                    url: "editProduct.php",
                    data: data,
                    beforeSend: function () {
                        $('.loader').show();
                    },
                    complete: function () {
                        $('.loader').hide();
                    },
                    success: function (data) {
                        location.reload();
                    }
                });
            } else {
                if ($('.inputEditProductS').is(":visible") || $('.inputEditFriendlyN').is(":visible")) {
                    if ($('#copyProdForm').valid()) {
                        var updatedProdFamily = $('.inputEditProductS').val();
                        var updatedProdName = $('.inputEditFriendlyN').val();
                        var updatedSerialNo = $('.serialN').val();
                        if (updatedProdFamily == '') {
                            updatedProdFamily = $('.productS').val();
                        }
                        $.ajax({
                            type: "POST",
                            url: "prod-owner.php",
                            data: {updateProdDetailsTable: 'true', updatedProdFamily: updatedProdFamily, updatedProdName: updatedProdName, updatedSerialNo: updatedSerialNo},
                            beforeSend: function () {
                                $('.updateProdDetailsLoader').show();
                            },
                            success: function (data) {
                                if ($('.noIemsMsg').length > 0) {
                                    $('.noIemsMsg').hide();
                                }
                                if ($('#prodList li:contains("' + updatedProdName + '")').length == 0) {
                                    var clones = $('#clonedLi').clone();
                                    $('#prodList').append(clones);
                                    $('#clonedLi').show();
                                    $('#clonedProdFName').html(updatedProdFamily.replace(/\s+/g, '-').toLowerCase());
                                    $('#clonedProdName').html(updatedProdName).append('&nbsp;<span class="label label-danger">pending</span>');
                                    $('#clonedSerialRange').html(updatedSerialNo);
                                    var liLength = "sw" + $('#prodList li.list-group-item').length;
                                    $('#clonedLi').find('.collapsed').attr('href', '#' + liLength);
                                    $('#clonedLi').find('.collapse').attr('id', liLength);
                                    $('#clonedLi').find('.badge').html('0');

                                    $.ajax({
                                        type: "POST",
                                        url: "getBlobs.php",
                                        data: {bindBlobToFileDD: updatedProdFamily.replace(/\s+/g, '-').toLowerCase()},
                                        success: function (data) {
                                            data = JSON.parse(data);
                                            var i;
                                            for (i = 0; i < data.length; ++i) {
                                                $('#prodList>li:last').find('ul.dropdown-menu').append('<li><a class="addFileDD" href="#" data-toggle="modal" data-target="#download">' + data[i] + '</a></li>');
                                            }
                                        }
                                    });
                                    $('#prodList').find('#clonedLi, #clonedProdFName, #clonedProdName, #clonedSerialRange').removeAttr('id');
                                }
                                $('.updateProdDetailsLoader').hide();
                            }
                        });
                        $.ajax({
                            type: "POST",
                            url: "prod-owner.php",
                            data: {saveProductFamily: 'true', updatedProdFamily: updatedProdFamily, updatedProdName: updatedProdName, updatedSerialNo: updatedSerialNo},
                            success: function (data) {
                                $('#product').modal('toggle');
                                $.each($('#prodList li'), function () {
                                    if ($(this).find('.clonedProdFName').text() == updatedProdFamily && $(this).find('.clonedProdName').text() == updatedProdName) {
                                        if ($(this).find('div:first').hasClass('collapsed')) {
                                            $(this).find('div:first').trigger('click');
                                            $('#prodList li:eq(' + $(this).index() + ')').focus();
                                        }
                                    }
                                });
                            }
                        });
                    }
                } else {
                    if ($('.noIemsMsg').length > 0) {
                        $('.noIemsMsg').hide();
                    }
                    var updatedProdFamily = $('#productS').val();
                    var updatedProdName = $('#friendlyN').val();
                    var updatedSerialNo = $('#serialN').val();
                    if ($('#prodList li:contains("' + updatedProdName + '")').length == 0) {
                        var clones = $('#clonedLi').clone();
                        $('#prodList').append(clones);
                        if ($('#copyProdForm').valid()) {
                            $('#clonedLi').show();
                            $('#clonedProdFName').html($('#productS').val().replace(/\s+/g, '-').toLowerCase());
                            $('#clonedProdName').html($('#friendlyN').val()).append('&nbsp;<span class="label label-danger">pending</span>');
                            $('#clonedSerialRange').html($('#serialN').val());
                            var liLength = "sw" + $('#prodList li.list-group-item').length;
                            $('#clonedLi').find('.collapsed').attr('href', '#' + liLength);
                            $('#clonedLi').find('.collapse').attr('id', liLength);
                            $('#clonedLi').find('.badge').html('0');

                            $.ajax({
                                type: "POST",
                                url: "getBlobs.php",
                                data: {bindBlobToFileDD: $(this).parent().parent().find('#productS').val().replace(/\s+/g, '-').toLowerCase()},
                                success: function (data) {
                                    data = JSON.parse(data);
                                    var i;
                                    for (i = 0; i < data.length; ++i) {
                                        $('#prodList>li:last').find('ul.dropdown-menu').append('<li><a class="addFileDD" href="#" data-toggle="modal" data-target="#download">' + data[i] + '</a></li>');
                                    }
                                }
                            });
                        }
                        $('#prodList').find('#clonedLi, #clonedProdFName, #clonedProdName, #clonedSerialRange').removeAttr('id');
                    }
                    $.ajax({
                        type: "POST",
                        url: "prod-owner.php",
                        data: {saveProductFamily: 'true', updatedProdFamily: updatedProdFamily, updatedProdName: updatedProdName, updatedSerialNo: updatedSerialNo},
                        beforeSend: function () {
                            $('.loader').show();
                        },
                        complete: function () {
                            $('.loader').hide();
                        },
                        success: function (data) {
                            $('#product').modal('toggle');
                            $.each($('#prodList li'), function () {
                                if ($(this).find('.clonedProdFName').text() == updatedProdFamily && $(this).find('.clonedProdName').text() == updatedProdName) {
                                    if ($(this).find('div:first').hasClass('collapsed')) {
                                        $(this).find('div:first').trigger('click');
                                        $('#prodList li:eq(' + $(this).index() + ')').focus();
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });


        $(document).on("change", '#copyProdForm .productS', function () {
            getDistinctProdNames();
        });
        $(document).on("change", '#copyProdForm .friendlyN', function () {
            getDistinctSno();
        });
        $('#copyProdForm').validate({
            errorClass: "my-error-class",
            validClass: "my-valid-class"
        });
        $.validator.addMethod("checkName", function (value, element)
        {
            return this.optional(element) || /^(([a-z\d]((-(?=[a-z\d]))|([a-z\d])){2,62})|(\$root))$/.test(value);
        }, "Container names may only contain lowercase letters, numbers, and hyphens, and must begin with a letter or a number. The name cannot contain two consecutive hyphens. The name must also be between 3 and 63 characters long.");
        $(".inputEditProductS").rules("add", {
            required: true,
            checkName: "checkName",
            messages: {
                required: "Required",
                checkName: "Container names may only contain lowercase letters, numbers, and hyphens, and must begin with a letter or a number. The name cannot contain two consecutive hyphens. The name must also be between 3 and 63 characters long."
            }
        });
        $(".inputEditfriendlyN").each(function () {
            $(this).rules("add", {
                required: true
            });
        });
        $(document).on("click", '.addNewFamilyLink', function () {
            $('.productS, .friendlyN, .addNewFamilyLink, .addNewProdLink, .cancelProdLink').hide();
            $('.inputEditProductS, .inputEditFriendlyN, .help-block, .cancelFamilyLink').show();
            $(".serialN").attr("readonly", false).val('');
        });
        $(document).on("click", '.cancelFamilyLink', function () {
            $('.inputEditProductS, .inputEditFriendlyN, .help-block, .cancelFamilyLink, .cancelProdLink').hide();
            $('.productS, .friendlyN, .addNewFamilyLink, .addNewProdLink').show();
            getDistinctSno();
            $(".serialN").attr("readonly", true);
            $('#copyProdForm').find('input').removeClass('my-error-class');
        });
        $(document).on("click", '.addNewProdLink', function () {
            $('.friendlyN, .addNewFamilyLink, .addNewProdLink, .cancelFamilyLink').hide();
            $('.inputEditFriendlyN, .help-block, .cancelProdLink').show();
            $('.pfBlk .help-block').hide();
            $(".serialN").attr("readonly", false).val('');
        });
        $(document).on("click", '.cancelProdLink', function () {
            $('.inputEditFriendlyN, .help-block, .cancelFamilyLink, .cancelProdLink').hide();
            $('.productS, .friendlyN, .addNewFamilyLink, .addNewProdLink').show();
            getDistinctSno();
            $(".serialN").attr("readonly", true);
            $('#copyProdForm').find('input').removeClass('my-error-class');
        });


        $(".productS, .friendlyN, .serialN, .inputEditProductS, .inputEditFriendlyN").each(function () {
            $(this).rules("add", {
                required: true
            });
        });
        $('#downloadModal').validate({
            errorClass: "my-error-class",
            validClass: "my-valid-class"
        });
//        $.validator.addMethod("noSpace", function (value, element) {
//            return value.indexOf(" ") < 0 && value != "";
//        }, "No space please and don't leave it empty");
//        $(".exampleInputFile").rules("add", {
//            required: true,
//            noSpace: "noSpace",
//            messages: {
//                required: "Required",
//                noSpace: "No space please and don't leave it empty"
//            }
//        });
        $(".modal_catDd, .modal_Friendlyname, .modal_versionNumber, .modal_inf, .exampleInputFile").each(function () {
            $(this).rules("add", {
                required: true
            });
        });
        $('#addExistingProdModalForm').validate({
            errorClass: "my-error-class",
            validClass: "my-valid-class"
        });
        $(".addExistingProdModal_catDd, .addExistingProdModal_Friendlyname, .addExistingProdModal_Filename, .addExistingProdModal_versionNumber, .addExistingProdModal_inf").each(function () {
            $(this).rules("add", {
                required: true
            });
        });
        var os;
        $('#addFileToProdBtn').on('click', function () {
            var clones = $('#clonedTbl').clone();
            if ($('#downloadModal').valid()) {
                clones.find('td a').removeClass('displayNone');
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody').append(clones.removeClass('displayNone'));
                if ($('input[name=inlineRadioOptions]:checked', '#downloadModal').val() == "option0") {
                    os = "Any";
                } else if ($('input[name=inlineRadioOptions]:checked', '#downloadModal').val() == "option1") {
                    os = "Windows";
                } else if ($('input[name=inlineRadioOptions]:checked', '#downloadModal').val() == "option2") {
                    os = "Mac OS";
                } else {
                    os = "Linux";
                }
//                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('#clonedTbl, #clonedTbl td a').removeClass('displayNone');
//                $('.count').html($('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr').index() + 1);
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('.badge').html($('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('table tbody tr').not('.displayNone').length);
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').find('.friendlyName').html($('#modal_Friendlyname').val());
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').find('.fileName').html($('#exampleInputFile').val().replace(/^.*[\\\/]/, ''));
//                filesToBlob.push($('#exampleInputFile').val().replace(/^.*[\\\/]/, ''));
//                var file = $.grep(selectedFiles, function (e) {
//                    return e.filename == $('#exampleInputFile').val().replace(/^.*[\\\/]/, '');
//                });
//                filesToBlob.push(file);
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').find('.os').html(os);
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').find('.lang').html($('#modal_lang').val());
//                $('#prodList').find('#clonedTbl').removeAttr('id');

                var file_data = $('#exampleInputFile').prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                var containerName = $('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('.clonedProdFName').text().trim();
                $(window).on('beforeunload', function () {
                    return 'Are you sure you want to leave?';
                });
                $.ajax({
                    url: 'uploadToBlob.php?prodSeries=' + containerName,
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    beforeSend: function () {
                        $('.loader').show();
                    },
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').append("<div class='downloadStatus'></div>");
                                $('.downloadStatus').css({'width': (percentComplete) + "%", 'max-width': $('.downloadStatus').parent().width()});
                                if (percentComplete === 100) {
                                    $('.downloadStatus').fadeIn(1000).fadeOut(1000);
                                    $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').addClass('downloadSuccess');
                                    var data = $('#downloadModal').serialize();
                                    data += "&prodFamily=" + encodeURIComponent($('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('.clonedProdFName').text().trim());
                                    $('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('.label').remove();
                                    data += "&prodName=" + encodeURIComponent($('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('.clonedProdName').text().trim());
                                    data += "&serialRange=" + encodeURIComponent($('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('.clonedSerialRange').text().trim());
                                    data += "&filename=" + encodeURIComponent($('#exampleInputFile').prop('files')[0]['name']);
                                    data += "&os=" + encodeURIComponent($('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last td.os').text().trim());
                                    $.post('uploadProdOwnerDetailsToDb.php?addExisting=false', data);
                                    $('#download').find("input,textarea").val('');
                                    //$('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').removeAttr('id').removeAttr('class');
                                    $('[data-toggle="tooltip"]').tooltip();
                                    $('[data-toggle="popover"]').popover();
                                }
                            }
                        }, false);
                        return xhr;
                        $('.downloadStatus').remove();
                    },
                    success: function (php_script_response) {
                        $(window).off('beforeunload');
                    },
                    complete: function (xhr, textStatus) {
                        console.log(xhr.status + " / " + textStatus);
                        $('.loader').hide();
                        location.reload();
                    }
                });
                $('#download').modal('toggle');
            }
        });

        var addExistingProdModalFormOS;
        $('#addExistingProdModalSave').on('click', function () {
            var clones = $('#clonedTbl').clone();
            if ($('#addExistingProdModalForm').valid()) {
                clones.find('td a').removeClass('displayNone');
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody').append(clones.removeClass('displayNone'));
                if ($('input[name=addExistingProdModalRadioOptions]:checked', '#addExistingProdModalForm').val() == "option0") {
                    addExistingProdModalFormOS = "Any";
                } else if ($('input[name=addExistingProdModalRadioOptions]:checked', '#addExistingProdModalForm').val() == "option1") {
                    addExistingProdModalFormOS = "Windows";
                } else if ($('input[name=addExistingProdModalRadioOptions]:checked', '#addExistingProdModalForm').val() == "option2") {
                    addExistingProdModalFormOS = "Mac OS";
                } else {
                    addExistingProdModalFormOS = "Linux";
                }
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('.badge').html($('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('table tbody tr').not('.displayNone').length);
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').find('.friendlyName').html($('#addExistingProdModal_Friendlyname').val());
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').find('.fileName').html($('#addExistingProdModal_Filename').val());
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').find('.os').html(addExistingProdModalFormOS);
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').find('.lang').html($('#addExistingProdModal_lang').val());
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').find('.infoIcon').attr('data-content', $('#addExistingProdModal_inf').val());

                var data = $('#addExistingProdModalForm').serialize();
                data += "&prodFamily=" + encodeURIComponent($('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('.clonedProdFName').text().trim());
                $('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('.label').remove();
                data += "&prodName=" + encodeURIComponent($('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('.clonedProdName').text().trim());
                data += "&serialRange=" + encodeURIComponent($('ul#prodList li.list-group-item:eq(' + addFileRoot + ')').find('.clonedSerialRange').text().trim());
                data += "&filename=" + encodeURIComponent($('#addExistingProdModal_Filename').val());
                data += "&os=" + encodeURIComponent($('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last td.os').text().trim());
                $.post('uploadProdOwnerDetailsToDb.php?addExisting=true', data);
                $('#addExistingProdModal').find("input,textarea").val('');
                //$('ul#prodList li.list-group-item:eq(' + addFileRoot + ') div.collapse table tbody tr:last').removeAttr('id').removeAttr('class');
                $('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle="popover"]').popover();

                $('#addExistingProdModal').modal('toggle');
            }
        });

        var copy_pName;
        var copy_pFName;
        var copy_sNo;
        $(document).on('click', '.addProdLink', function () {
            $('#product').find('#addProdBtn').text('Save');
        });
        $(document).on('click', '.editLi', function (e) {
            e.stopPropagation();
            $('#product').modal('toggle');
            copy_pName = $(this).parent().parent().find('.clonedProdName').text().trim();
            copy_pFName = $(this).parent().parent().find('.clonedProdFName').text().trim();
            copy_sNo = $(this).parent().parent().find('.clonedSerialRange').text().trim();
            $('#friendlyN').val(copy_pName);
            $('#productS').val(copy_pFName);
            $('#serialN').val(copy_sNo);
            $('#product').find('#addProdBtn').text('Update');
        });
        $(document).on('click', '.copyLi', function (e) {
            e.stopPropagation();
            $('#product').modal('toggle');
            copy_pName = $(this).parent().parent().find('.clonedProdName').text().trim();
            copy_pFName = $(this).parent().parent().find('.clonedProdFName').text().trim();
            copy_sNo = $(this).parent().parent().find('.clonedSerialRange').text().trim();
            $('#friendlyN').val(copy_pName);
            $('#productS').val(copy_pFName);
            $('#serialN').val(copy_sNo);
            $('#product').find('#addProdBtn').text('Copy');
        });
        $(document).on('click', '.deleteLi', function (e) {
            e.stopPropagation();
            $('.containerNameToDelete').html('Delete Product: <strong>' + $(this).closest('li').find('.clonedProdName').text().trim() + '</strong>');
            $('.blobNameToDelete').hide();
            $('.containerNameToDelete').show();
            deleteContainerFlag = true;
        });
        $(document).on('click', '.addFileDD', function (e) {
            $('#modal_catDd').val($('#modal_catDd > option:eq(' + $(this).parent().index() + ')').val());
            if ($(this).parent().index() > 2) {
                $('#addExistingProdModal_Filename').val($(this).text().trim());
            }
        });
    });
    $("#prodList").bind("DOMSubtreeModified", function () {
        $('#prodList').find('ul li a').unbind().click(function () {
            addFileRoot = $(this).closest('li.list-group-item').index();
        });
    });
    $(document).on('DOMNodeInserted', $('.popover'), function () {
        $('.popover').css({'left': $('.infoIcon').position().left - 131});
    });
    var clonedRow;
    $("body").bind("DOMSubtreeModified", function () {
        var txt;
        var liIndex;
        var trIndex;

        $('.prodUpdateCancelBtn').click(function () {
            $(this).parent().closest('tr').html('').addClass('retainRow');
            $('.retainRow').html(clonedRow.html());
            $(function () {
                $('[data-toggle="popover"]').popover();
            });
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
            $('.retainRow .editProdContent i').attr({
                'title': 'edit'
            });
            $('.retainRow').removeAttr('class');
        });
        $('.editProdContent').unbind().click(function () {
            clonedRow = $(this).closest('tr').clone();
            txt = $(this).closest('tr').find('td:eq(1)').text();
            var selectedOS = $(this).closest('tr').find('.osTd').text();
            var selectedLang = $(this).closest('tr').find('.langNameTd').text();
            if (txt == '') {
                txt = $(this).closest('tr').find('td:eq(1) input').val();
            }
            $(this).closest('tr').find('td:eq(1)').text('');
            $(this).closest('tr').find('td:eq(1)').append('<input type="text" class="form-control friendlyNameTxt" value="' + txt + '" />');
//            $(this).parent().css('vertical-align', 'bottom');
//            $(this).find('i').toggleClass('fa-pencil fa-save').attr('id', 'changeProdValues').addClass('updateFreindlyName').css('margin-left', '0px');
            $(this).closest('tr').find('.osTd').html('<select class="form-control osDD" name="osDD">' +
                    '<option value="Any">Any</option>' +
                    '<option value="Windows">Windows</option>' +
                    '<option value="Mac OS">Mac OS</option>' +
                    '<option value="Linux">Linux</option>' +
                    '</select>');
            $(this).closest('tr').find('.langNameTd').html('<select class="form-control langDD" name="langDD">' +
                    '<option value="Not language specific">Not language specific</option>' +
                    '<option value="English">English</option>' +
                    '<option value="Spanish">Spanish</option>' +
                    '<option value="French">French</option>' +
                    '</select>');
            $('.osDD').val(selectedOS);
            $('.langDD').val(selectedLang);
            var clonedContent = '<div class="prodUpdateBtn"><button class="btn btn-success prodUpdateSaveBtn"><i class="fa fa-fw fa-save pull-left"></i>Save</button><button class="btn btn-danger prodUpdateCancelBtn"><i class="fa fa-fw fa-close pull-left"></i></button></div>';
            $(this).parent().html(clonedContent);
        });
        $('.prodUpdateSaveBtn').unbind().click(function () {
//            if ($(this).hasClass('fa-save') && $(this).closest('tr').find('.friendlyNameTd input').val() != '') {
            trIndex = $(this).parent().parent().index() + 1;
            liIndex = $(this).closest('li').index();
            var pf = $(this).closest('li').find('.clonedProdFName').text();
            var changedFN = $(this).closest('tr').find('.friendlyNameTd input').val();
            var currentFileName = $(this).closest('tr').find('.fileNameTd').text();
            var changedOS = $(this).closest('tr').find('.osDD').val();
            var changedLang = $(this).closest('tr').find('.langDD').val();
            $.ajax({
                type: "POST",
                url: "prod-owner.php",
                data: {pf: pf, updateFreindlyNameVal: changedFN, currentFileName: currentFileName, changedOS: changedOS, changedLang: changedLang},
                beforeSend: function () {
                    $('.loader').show();
                },
                complete: function () {
                    $('.loader').hide();
                },
                success: function (data) {
                    $('ul#prodList').find('li.list-group-item:eq(' + liIndex + ')').find('.friendlyNameTxt').css({'background': 'transparent', 'border': '0px'});
                    $('ul#prodList').find('li.list-group-item:eq(' + liIndex + ')').find('.friendlyNameTxt').next().hide();
                    location.reload();
                }
            });
//            }
        });
        $('.deleteProdContent').unbind().click(function () {
            $('.blobNameToDelete').html('Delete File: <strong>' + $(this).parent().parent().find('.fileNameTd').text().trim() + '</strong>');
            $('.containerNameToDelete').hide();
            $('.blobNameToDelete').show();
            $('.deleteRowProdSeries').text($(this).closest('li').find('.clonedProdFName').text());
            $('.deleteRowProdName').text($(this).closest('li').find('.clonedProdName').text());
            deleteContainerFlag = false;
        });
    });

    $('.deleteRowFromProdListContent').on('click', function () {
        var deleteContainer = $('.containerNameToDelete strong').text().trim();
        if (deleteContainerFlag) {
            if (deleteContainer != '') {
                $.ajax({
                    type: "POST",
                    url: "prod-owner.php",
                    data: {deleteContainer: deleteContainer},
                    beforeSend: function () {
                        $('.loader').show();
                    },
                    complete: function () {
                        $('.loader').hide();
                    },
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        } else {
            var deleteSingleItem = $('.blobNameToDelete strong').text().trim();
            var deleteBlobName = $('.containerNameToDelete strong').text().trim();
            var deleteRowProdSeries = $('.deleteRowProdSeries').text();
            var deleteRowProdName = $('.deleteRowProdName').text();
            if (deleteSingleItem != '') {
                $.ajax({
                    type: "POST",
                    url: "prod-owner.php",
                    data: {deleteRowFromProdListContent: deleteSingleItem, deleteBlobName: deleteBlobName, deleteRowProdSeries: deleteRowProdSeries, deleteRowProdName: deleteRowProdName},
                    beforeSend: function () {
                        $('.loader').show();
                    },
                    complete: function () {
                        $('.loader').hide();
                    },
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        }
    });
</script>
</body>
</html>
