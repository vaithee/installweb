<!DOCTYPE html>
<head>
    <title>Kodak Alaris::Information Management</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/ico" href="images/favicon.ico">
    <link rel="icon" type="image/x-icon" href="images/favicon.ico">
    <link rel="icon" type="image/png" href="images/favicon.ico">
    <link href="css/pingendo-bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="css/KodakAlaris_IM_bootstrap.css">
    <link href="css/kodak_style.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-2.2.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.validate.js" type="text/javascript"></script>
    <script src="js/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="css/fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="js/custom.js" type="text/javascript"></script>
    <script src="js/fileDownload.js" type="text/javascript"></script>
    <script src="js/date.min.js" type="text/javascript"></script>
    <script src='js/jquery.cookie.js' type="text/javascript"></script>
</head>
<body>
    <?php
    include 'header.php';
    include("config.php");
    header('Set-Cookie: fileDownload=true; path=/');
    header("Cache-Control", "no-cache, no-store, must-revalidate");
    $BASE_PATH = parse_url(strtok("https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", '?'));
    $BASE_PATH = "https://" . $BASE_PATH["host"];
    $product_id = base64_decode(urldecode($_GET['pid']));
    $startdate = base64_decode(urldecode($_GET['sd']));
    $enddate = base64_decode(urldecode($_GET['ed']));
    $emailId = base64_decode(urldecode($_GET['eid']));

    ini_set('memory_limit', '-1');

    require_once 'WindowsAzure/WindowsAzure.php';
    require_once 'HTTP/Request2.php';

    use WindowsAzure\Blob\Models\BlobBlockType;
    use WindowsAzure\Blob\Models\Block;
    use WindowsAzure\Blob\Models\CommitBlobBlocksOptions;
    use WindowsAzure\Blob\Models\CreateBlobOptions;
    use WindowsAzure\Blob\Models\CreateContainerOptions;
    use WindowsAzure\Blob\Models\ListContainersOptions;
    use WindowsAzure\Blob\Models\PublicAccessType;
    use WindowsAzure\Common\Internal\IServiceFilter;
    use windowsazure\common\Internal\Resources;
    use WindowsAzure\Common\ServiceException;
    use WindowsAzure\Common\ServicesBuilder;

$connectionString = 'DefaultEndpointsProtocol=https;AccountName=iminteractiveinstall2199;AccountKey=77uYvNagWxCt5V2JTsk+FCAoADdx+O1NoNrHLPXdX8LQjr9mmj+8tiAQ33bONA304QcRT8OEtZun1oaqC0BEYw==';
    $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);

//    $content = fopen("files/i1190.jpg", "r");
//    $blob_name = "scanmate-i940.jpg";
//    $blobRestProxy->createBlockBlob("i940", $blob_name, $content);
    //   error_reporting(E_ALL);
    // ini_set('display_errors', 1);

    try {
        $productId = base64_decode(urldecode($_GET['pid']));
        echo "product Id:" . $productId;
        $get_product_details = "SELECT product_family FROM [ciotiswd].product_details where id = " . $productId;
        $stmt = sqlsrv_query($conn, $get_product_details);

        while ($row = sqlsrv_fetch_array($stmt)) {
            $modelNo = $row['product_family'];
            $modelNo = explode(" ", $row['product_family'])[0] . "-" . strtolower(explode(" ", $row['product_family'])[1]);
            $modelNo = strtolower($modelNo);
        }

        echo "model number :" . $modelNo;
        // List blobs.
        $blob_list = $blobRestProxy->listBlobs($modelNo);
        $blobs = $blob_list->getBlobs();
        //                            echo "<div>" . $blob->getName() . "<a href=" . $blob->getUrl() . "> " . $blob->getName() . "</a><div/>";
    } catch (ServiceException $e) {
        $code = $e->getCode();
        $error_message = $e->getMessage();
        $code . ": " . $error_message . "<br />";
    }

//    getting source from blob storage Azure

    if (isset($_POST['files'])) {
        $error = ""; //error holder
        if (isset($_POST['createzip'])) {
            $post = $_POST;
            $file_folder = "https://iminteractiveinstall2199.blob.core.windows.net/" . $modelNo . "/"; // folder to load files
            echo "file folder: " . $file_folder;
            if (extension_loaded('zip')) {
// Checking ZIP extension is available
                if (isset($post['files']) and count($post['files']) > 0) {
// Checking files are selected
                    $zip = new ZipArchive(); // Load zip library 
                    $zip_name = time() . ".zip"; // Zip name
                    if ($zip->open($zip_name, ZIPARCHIVE::CREATE) !== TRUE) {
                        // Opening zip file to load files
                        $error .= "* Sorry ZIP creation failed at this time";
                    }
                    foreach ($post['files'] as $file) {
                        $zip->addFile($file_folder . $file); // Adding files into zip
                        $content = file_get_contents($file_folder . $file);
                        $zip->addFromString($file, $content);
                        $zip->setCompressionIndex(-1, ZipArchive::CM_STORE);
                        $zip->setCompressionIndex(-1, ZipArchive::CM_DEFLATE);
//                        require_once('pclzip.lib.php');
//                        $archive = new PclZip('archive.zip');
//                        $v_list = $archive->create($file_folder . $file);
//                        if ($v_list == 0) {
//                            die("Error : " . $archive->errorInfo(true));
//                        }
                    }
                    $zip->close();
                    if (file_exists($zip_name)) {
// push to download the 
                        header('Content-Type: application/x-gzip');
                        header("Pragma: public");
                        header("Expires: 0");
                        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                        header("Cache-Control: public");
                        header("Content-Description: File Transfer");
                        //We can likely use the 'application/zip' type, but the octet-stream 'catch all' works just fine.  
                        header("Content-type: application/octet-stream");
                        header("Content-Disposition: attachment; filename='$zip_name'");
                        header("Content-Transfer-Encoding: binary");
                        header("Content-Type:mime/type");
                        header("Content-Length: " . filesize($file));

                        while (ob_get_level()) {
                            ob_end_clean();
                        }

                        @readfile($zip_name);
                        ob_end_flush();

// remove zip file is exists in temp path
                        unlink($zip_name);

//                        $file_url = 'archive.zip';
//                        header('Content-Type: application/zip');
//                        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
//                        readfile($file_url);
                    }
                } else
                    $error .= "* Please select file to zip ";
            } else
                $error .= "* You dont have ZIP extension";
        }
    }

    function formatBytes($size, $precision = 2) {
        $base = log($size, 1024);
        $suffixes = array('', 'KB', 'MB', 'GB', 'TB');
        return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)];
    }

    if (!empty($_POST["filename"])) {
        $prod_id = $_POST['prod_id'];
        $filename = $_POST['filename'];
        $date = $_POST['date'];
        $download_count = $_POST['download_count'];
        $sql_prod_name = "SELECT model_number FROM [ciotiswd].product_details where id = '$prod_id'";
        $res_prod_name = sqlsrv_query($conn, $sql_prod_name);
        $row_prod_name = sqlsrv_fetch_array($res_prod_name);
        $row_product = strtolower($row_prod_name['model_number']);
        $report_sql = "IF NOT EXISTS (SELECT * FROM [ciotiswd].reports where filename='$filename' and product_name = '$row_product') BEGIN INSERT INTO [ciotiswd].reports(filename, date, download_count, eligiblescan, attachrate, product_name) VALUES ('$filename', '$date', '$download_count', (SELECT COUNT(product_id) FROM [ciotiswd].customers WHERE product_id = '$prod_id'), '100', '$row_product') END ELSE BEGIN UPDATE [ciotiswd].reports SET eligiblescan=(select count(product_id) FROM [ciotiswd].customers WHERE product_id = '$prod_id') , download_count = download_count + 1 where filename='$filename' and product_name = '$row_product' END" or die(print_r(sqlsrv_errors(), true) . "q1");
        sqlsrv_query($conn, $report_sql);

        $percentagesql = "UPDATE Table_A SET Table_A.attachrate = (Table_B.download_count * 100)/Table_B.eligiblescan FROM [ciotiswd].reports AS Table_A INNER JOIN [ciotiswd].reports AS Table_B ON Table_A.id = Table_B.id";
        sqlsrv_query($conn, $percentagesql);

        //$prod_owner_list_id = "update q set q.product_name = b.product_name from [ciotiswd].reports q inner join [ciotiswd].product_owner_list_content a on q.filename = a.friendly_name inner join [ciotiswd].product_owner_list b on a.product_owner_list_id = b.id";
        //sqlsrv_query($conn, $prod_owner_list_id);
    }
    ?>
    <div class="cover">
        <input type="hidden" name="prodId" id="prodId" value="<?php echo $productId; ?>" >
        <div class="cover-image" style="background-image : url('images/data.png'); background-color:rgb(25,45,105);"></div>
        <div class="container" style="margin-bottom: 70px;">
            <div class="row">
                <form role="form" method="post" id="downloadForm">
                    <?php if ($enddate > $startdate) { ?>
                        <div class="col-md-8 col-md-offset-2">
                            <h3 style="color: #fff;"><?php echo $lang['choose_software']; ?></h3>
                            <div class="row filterOptions">
                                <div class="col-md-6">
                                    <ul class="nav nav-pills filterProdByOS">
                                        <li class="active"><a data-toggle="tab" href="#windowsProds">Windows</a></li>
                                        <li><a data-toggle="tab" href="#macProds">Mac</a></li>
                                        <li><a data-toggle="tab" href="#linuxProds">Linux</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="windowsProds">
                                    <?php
                                    if (isset($blobs) && sizeof($blobs) > 0) {
                                        $WinDataContent = 0;
                                        foreach ($blobs as $key => $blob) {
                                            $get_file_data = "select category, friendly_name, description from [ciotiswd].product_owner_list_content where product_owner_list_id = (select id from [ciotiswd].product_owner_list where product_name LIKE '%' + (select model_number from [ciotiswd].product_details where id='" . $productId . "') + '%') and  product_family = '" . $modelNo . "' and filename = '" . $blob->getName() . "' and os in ('Windows','Any')";
                                            $getFileData = sqlsrv_query($conn, $get_file_data);
                                            $rows = sqlsrv_has_rows($getFileData);
                                            if ($rows === true) {
                                                $WinDataContent++;
                                            }
                                            while ($row = sqlsrv_fetch_array($getFileData)) {
                                                $productDesc = $row['description'];
                                                $productCat = $row['category'];
                                                ?>
                                                <div class="list-group-item form-group">
                                                    <label class = "h4">
                                                        <?php echo $productFN = $row['friendly_name'];
                                                        ?>
                                                        <span class="text-muted h5">
                                                            (<?php
                                                            $propResult = $blobRestProxy->getBlobProperties($modelNo, $blob->getName(), null);
                                                            if (isset($productCat) && $productCat != null) {
                                                                echo $productCat . " - " . formatBytes($propResult->getProperties()->getContentLength());
                                                            }
                                                            ?>)
                                                        </span>
                                                    </label>
                                                    <div class="pull-right">
                                                        <a class="btn btn-primary download_icon" onclick="downloadFile('<?php echo $blob->getUrl(); ?>')" data-toggle="tooltip" data-placement="top" title="start download" data-loading-text="Download"><span class="glyphicon glyphicon-download down_load_icon "></span><?php echo $lang['download']; ?></a>
                                                        <a class="btn btn-xs btn-default glyphicon glyphicon-info-sign pull-right download_info" data-toggle="collapse" href="#collapseWin<?php echo $key; ?>" aria-expanded="false" aria-controls="#collapseWin<?php echo $key; ?>"></a>
                                                    </div>
                                                    <div class="collapse" id="collapseWin<?php echo $key; ?>">
                                                        <?php
                                                        if (isset($productDesc) && $productDesc != null) {
                                                            echo $productDesc;
                                                        } else {
                                                            echo "No description available!";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                        if ($WinDataContent == 0) {
                                            echo "<div class='emptyContentDiv'>Items yet to be added!</div>";
                                        }
                                    } else {
                                        echo "<div class='emptyContentDiv'>Items yet to be added!</div>";
                                    }
                                    ?>
                                </div>
                                <div class="tab-pane fade" id="macProds">
                                    <?php
                                    if (isset($blobs) && sizeof($blobs) > 0) {
                                        $MacDataContent = 0;
                                        foreach ($blobs as $key => $blob) {
                                            $get_file_data = "select category, friendly_name, description from [ciotiswd].product_owner_list_content where product_owner_list_id = (select id from [ciotiswd].product_owner_list where product_name LIKE '%' + (select model_number from [ciotiswd].product_details where id='" . $productId . "') + '%') and  product_family = '" . $modelNo . "' and filename = '" . $blob->getName() . "' and os in ('Mac OS','Any')";
                                            $getFileData = sqlsrv_query($conn, $get_file_data);
                                            $rows = sqlsrv_has_rows($getFileData);
                                            if ($rows === true) {
                                                $MacDataContent++;
                                            }
                                            while ($row = sqlsrv_fetch_array($getFileData)) {
                                                $productDesc = $row['description'];
                                                $productCat = $row['category'];
                                                ?>
                                                <div class="list-group-item form-group">
                                                    <label class = "h4">
                                                        <?php echo $productFN = $row['friendly_name'];
                                                        ?>
                                                        <span class="text-muted h5">
                                                            (<?php
                                                            $propResult = $blobRestProxy->getBlobProperties($modelNo, $blob->getName(), null);
                                                            if (isset($productCat) && $productCat != null) {
                                                                echo $productCat . " - " . formatBytes($propResult->getProperties()->getContentLength());
                                                            }
                                                            ?>)
                                                        </span>
                                                    </label>
                                                    <div class="pull-right">
                                                        <a class="btn btn-primary download_icon" onclick="downloadFile('<?php echo $blob->getUrl(); ?>')" data-toggle="tooltip" data-placement="top" title="start download" data-loading-text="Download"><span class="glyphicon glyphicon-download down_load_icon "></span>Download</a>
                                                        <a class="btn btn-xs btn-default glyphicon glyphicon-info-sign pull-right download_info" data-toggle="collapse" href="#collapseMac<?php echo $key; ?>" aria-expanded="false" aria-controls="#collapseMac<?php echo $key; ?>"></a>
                                                    </div>
                                                    <div class="collapse" id="collapseMac<?php echo $key; ?>">
                                                        <?php
                                                        if (isset($productDesc) && $productDesc != null) {
                                                            echo $productDesc;
                                                        } else {
                                                            echo "No description available!";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                        if ($MacDataContent == 0) {
                                            echo "<div class='emptyContentDiv'>Items yet to be added!</div>";
                                        }
                                    } else {
                                        echo "<div class='emptyContentDiv'>Items yet to be added!</div>";
                                    }
                                    ?>
                                </div>
                                <div class="tab-pane fade" id="linuxProds">
                                    <?php
                                    if (isset($blobs) && sizeof($blobs) > 0) {
                                        $LinDataContent = 0;
                                        foreach ($blobs as $key => $blob) {
                                            $get_file_data = "select category, friendly_name, description from [ciotiswd].product_owner_list_content where product_owner_list_id = (select id from [ciotiswd].product_owner_list where product_name LIKE '%' + (select model_number from [ciotiswd].product_details where id='" . $productId . "') + '%') and  product_family = '" . $modelNo . "' and filename = '" . $blob->getName() . "' and os in ('Linux','Any')";
                                            $getFileData = sqlsrv_query($conn, $get_file_data);
                                            $rows = sqlsrv_has_rows($getFileData);
                                            if ($rows === true) {
                                                $LinDataContent++;
                                            }
                                            while ($row = sqlsrv_fetch_array($getFileData)) {
                                                $productDesc = $row['description'];
                                                $productCat = $row['category'];
                                                ?>
                                                <div class="list-group-item form-group">
                                                    <label class = "h4">
                                                        <?php echo $productFN = $row['friendly_name'];
                                                        ?>
                                                        <span class="text-muted h5">
                                                            (<?php
                                                            $propResult = $blobRestProxy->getBlobProperties($modelNo, $blob->getName(), null);
                                                            if (isset($productCat) && $productCat != null) {
                                                                echo $productCat . " - " . formatBytes($propResult->getProperties()->getContentLength());
                                                            }
                                                            ?>)
                                                        </span>
                                                    </label>
                                                    <div class="pull-right">
                                                        <a class="btn btn-primary download_icon" onclick="downloadFile('<?php echo $blob->getUrl(); ?>')" data-toggle="tooltip" data-placement="top" title="start download" data-loading-text="Download"><span class="glyphicon glyphicon-download down_load_icon "></span>Download</a>
                                                        <a class="btn btn-xs btn-default glyphicon glyphicon-info-sign pull-right download_info" data-toggle="collapse" href="#collapseLinux<?php echo $key; ?>" aria-expanded="false" aria-controls="#collapseLinux<?php echo $key; ?>"></a>
                                                    </div>
                                                    <div class="collapse" id="collapseLinux<?php echo $key; ?>">
                                                        <?php
                                                        if (isset($productDesc) && $productDesc != null) {
                                                            echo $productDesc;
                                                        } else {
                                                            echo "No description available!";
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                        if ($LinDataContent == 0) {
                                            echo "<div class='emptyContentDiv'>Items yet to be added!</div>";
                                        }
                                    } else {
                                        echo "<div class='emptyContentDiv'>Items yet to be added!</div>";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-md-6 col-md-offset-3 expiredDownloadBlk">                            
                            <h2 style="font-size: 54px;"><?php echo $lang['link_expired']; ?></h2>
                            <p style="font-size: 18px;"><?php echo $lang['expire_days']; ?></p>
                            <?php if (empty($emailId)) { ?>
                                <a href="<?php echo $BASE_PATH; ?>" style='color: #3DAEA6;font-size: 18px;'><?php echo $lang['request_again']; ?></a>
                            <?php } else { ?>
                                <p style="font-size: 18px;"><?php echo $lang['new_link']; ?>: <i style="color: #3DAEA6;font-style: italic;"><?php echo $emailId; ?></i></p>
                                <a type="button" href="<?php echo "serialNumberVerify.php?reSendEmailId=" . $emailId . "&p_id=" . $product_id ?>" class="btn btn-block btn-lg btn-primary ResendEmail"><?php echo $lang['send_link']; ?></a>
                                <p style="font-size: 18px;margin-bottom: 0;"><?php echo $lang['or']; ?></p>
                                <a href="<?php echo $BASE_PATH; ?>" style='color: #3DAEA6;font-size: 18px;'><?php echo $lang['different_email']; ?></a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
    <div class="loader"></div>
</body>
</html>
