<?php

header('Content-Type: text/html; charset=utf-8');
include("config.php");

$tsql = "SELECT * FROM [ciotiswd].languages where status='active'";
$stmt = sqlsrv_query($conn, $tsql);
$lang_list = array();

while ($row = sqlsrv_fetch_array($stmt)) {
    $lang_list[] = $row;
}
echo json_encode($lang_list);
?>