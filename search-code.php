<script type="text/javascript">
    $(document).ready(function () {
        function showValues() {
            $("#results").css("opacity", 0.5);
            $("#loaderID").css("opacity", 1);

            var mainarray = new Array();

            var search_criteria_array = $("#search_criteria").val();
            var search_critiya_checklist = "&search_criteria=" + search_criteria_array;


            var product_name_array = $("#product_name").val();
            var product_name_checklist = "&product_name=" + product_name_array;


            var category_array = $("#category").val();
            var category_checklist = "&category=" + category_array;

            var daterangeStart = $("input[name='daterangepicker_start']").val();
            var daterangeEnd = $("input[name='daterangepicker_end']").val();
            var date = new Date(daterangeStart);
            var startdate = date.toString('yyyy/MM/dd');
            var date = new Date(daterangeEnd);
            var enddate = date.toString('yyyy/MM/dd');

            var daterange_array = startdate + ' - ' + enddate;
            if (daterange_array.indexOf('0NaN') > -1) {
                var daterange_checklist = "&daterange=";
            } else {
                var daterange_checklist = "&daterange=" + daterange_array;
            }

            var main_string = search_critiya_checklist + product_name_checklist + category_checklist + daterange_checklist;
            main_string = main_string.substring(1, main_string.length)
            $.ajax({
                type: "POST",
                url: "reportajaxnew.php",
                data: main_string,
                cache: false,
                success: function (html) {
                    //alert(html);
                    $("#results").html(html);
                    $("#results").css("opacity", 1);
                    $("#loaderID").css("opacity", 0);
                    $('.hide_accordion:empty').closest(".row.list").hide();
                    var norecord1 = $(".no_record1").text();
                    if (norecord1 == "NO RECORDS FOUND")
                    {
                        $(".export").attr("disabled", true);
                    } else {
                        $(".export").attr("disabled", false);
                    }
                }
            });


        }
        $(".cancelBtn").click(function () {
            window.location.href = window.location.href.split('?')[0];
        });

        $("#search_criteria").on("keyup", showValues);
        $(".applyBtn").on("click", showValues);
        $("select").on("change", showValues);


        $("#clear_search").click(function () {
            $('input[name="daterange"]').daterangepicker('reset');
            $('#category').prop('selectedIndex', 0);
            $('#search_criteria').val('');
            $('#product_name').prop('selectedIndex', 0);
            showValues();
        });

    });

</script>
