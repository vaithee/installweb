<!DOCTYPE html>
<head>
    <title>Kodak Alaris::Information Management</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/ico" href="images/favicon.ico">
    <link rel="icon" type="image/x-icon" href="images/favicon.ico">
    <link rel="icon" type="image/png" href="images/favicon.ico">
    <link href="css/pingendo-bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="css/KodakAlaris_IM_bootstrap.css">
    <link href="css/kodak_style.css" rel="stylesheet" type="text/css"/>
    <link href="css/fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="js/jquery-2.2.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.validate.js" type="text/javascript"></script>
    <script src="js/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/custom.js" type="text/javascript"></script>
</head>

<body>
    <?php
    include 'header.php';
//    session_start();
    include("config.php");
    if (isset($_COOKIE['product_id'])) {
        unset($_COOKIE['product_id']);
    }
    if (isset($_POST['serialNo'])) {
        $serial_no = $_POST['serialNo'];

        $product_id = "";
        $getProductId = "SELECT id,model_number FROM [ciotiswd].product_details where serial_no = '" . $serial_no . "'";

        $stmt = sqlsrv_query($conn, $getProductId);
        $product_id = array();
        while ($row = sqlsrv_fetch_array($stmt)) {
            $product_id = $row['id'];
            $model_number = $row['model_number'];
        }
        if (isset($model_number) && ($model_number != null)) {
            setcookie('product_id', $product_id, time() + (3600 * 24 * 30), '/', $_SERVER['SERVER_NAME'], true, false);
            setcookie('model_number', $model_number, time() + (3600 * 24 * 30), '/', $_SERVER['SERVER_NAME'], true, false);
        } else {
            setcookie('product_id', '', time() + (3600 * 24 * 30), '/', $_SERVER['SERVER_NAME'], true, false);
            setcookie('model_number', '', time() + (3600 * 24 * 30), '/', $_SERVER['SERVER_NAME'], true, false);
        }
        //setcookie('product_id', $product_id, time() + (3600 * 24 * 30));
        //setcookie('model_number', $model_number, time() + (3600 * 24 * 30));
    }
    ?>
    <div class="cover">
        <div class="cover-image" style="background-image : url('images/data.png'); background-color:rgb(25,45,105);"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center" style="text-shadow: 0 2px 8px #000;">
                    <h1 class="text-inverse"><?php echo $lang['head_lbl_1'] ?> <br class="hidden-sm hidden-xs"><?php echo $lang['head_lbl_2'] ?></h1>
                    <p class="text-inverse"><?php echo $lang['head_cap'] ?></p>
                </div>
            </div>
            <div class="row">
                <?php
                if (!empty($_POST['sure'])) {
                    $sure = $_POST['sure'];
                    if ($sure == 1) {
                        session_destroy();
                    }
                }
                extract($_POST);
                if (!empty($_GET['status'])) {
                    $status = $_GET['status'];
                    if ($status == "success") {
                        ?>
                        <div id="resendMailConfirmDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="panel panel-primary" id="formReq">
                                            <div class="panel-body successCont">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h3 class="text-center">Check your email</h3>
                                                <p>We sent a link to <i class="text-primary"><?php echo $_SESSION['email_id']; ?></i>. <p>If you don't see it within 10 minutes, check your spam, clutter or junk folder. </p><p>Still didn't receive it?  <a href="https://www.kodakalaris.com/b2b/support-center">Visit our Support Center</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        echo '<script>$( function() { $("#resendMailConfirmDialog").modal("show"); });</script>';
                    }
                }
                ?>
                <div class="col-md-offset-4 col-md-4 pad0">
                    <form role="form" id="indexForm" method="post">
                        <div class="panel panel-primary" id="formReq">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="control-label"><?php echo $lang['email'] ?></label>
                                    <input type="email" class="form-control inputEmail" id="inputEmail" name="inputEmail" placeholder="<?php echo $lang['placeholder_email']; ?>">
                                </div>
                                <div class="form-group seRial">
                                    <label for="inputSerial" class="control-label"><?php echo $lang['scanner_serial_no'] ?> &nbsp;</label>
                                    <button title="" id="popo" tabindex="-1" data-container="body" data-content="<?php echo $lang['tooltip_content']; ?>" type="button" data-trigger="focus" data-toggle="popover" class="btn btn-xs btn-link" data-placement="right" data-original-title="<?php echo $lang['tooltip_title']; ?>"><?php echo $lang['find_it'] ?>
                                        <i class="fa fa-fw fa-question-circle"></i>
                                    </button>
                                    <input type="text" class="form-control inputSerial" id="inputSerial" name="inputSerial" placeholder="12345678" autocomplete="off">
                                    <div id="product-status" class="glyphicon"></div>
                                    <!--<p><img src="images/widget-loader-lg.gif" id="loaderIcon" /></p>-->
                                    <div class="validModelNo">
                                        <?php echo $lang['model_Number']; ?>: <span></span>
                                    </div>
                                </div>
                                <button type="submit" value='1' class="btn btn-block btn-lg btn-primary" id="btnReq"  disabled="disabled"><?php echo $lang['download_link'] ?></button>
                                <div class="phy_disk" style="display:none"><a href="media.php"><i class="fa fa-fw fa-arrow-circle-right"></i><?php echo $lang['physical_disk'] ?></a></div>
                            </div>
                            <div class="panel-footer">
                                <label>
                                    <input type="checkbox" name="updatesNotify" value="1" checked="" class="check_pos" style='display:none'>
                                    <span class="p_notify"><?php echo $lang['notify']; ?></span>
                                </label>
                                <div>
                                    <i class="fa fa-fw fa-lock"></i><?php echo $lang['privacy_policy_1'] ?>
                                    <a href="https://www.kodakalaris.com/go/newprivacy" target="_blank"><?php echo $lang['privacy_policy_2'] ?></a>.
                                </div>
                            </div>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>  
    <div class="loader"></div>
    <div class="modal fade" id="confirmRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="panel panel-primary" id="formReq">
                        <div class="panel-body successCont">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="text-center"><?php echo $lang['emailCheck']; ?></h3>
                            <p><?php echo $lang['sent_link']; ?><i class="text-primary modalMailId"></i>. <p><?php echo $lang['spam_mail']; ?></p><p><?php echo $lang['visit1']; ?><a href="https://www.kodakalaris.com/b2b/support-center"><?php echo $lang['visit2']; ?></a>.</p>
                        </div>
                        <div class="panel-body errorCont">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h3 class="text-center">Invalid Serial No</h3>
                            <p>This Serial number is not available.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
