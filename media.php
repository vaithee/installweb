<!DOCTYPE html>
<head>
    <title>Kodak Alaris::Information Management</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/ico" href="images/favicon.ico">
    <link rel="icon" type="image/x-icon" href="images/favicon.ico">
    <link rel="icon" type="image/png" href="images/favicon.ico">
    <link href="css/pingendo-bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="css/KodakAlaris_IM_bootstrap.css">
    <link href="css/kodak_style.css" rel="stylesheet" type="text/css"/>
    <link href="css/fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="js/jquery-2.2.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.validate.js" type="text/javascript"></script>
    <!--<script src="js/validate.js" type="text/javascript"></script>-->
    <script src="js/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/custom.js" type="text/javascript"></script>
</head>
<?php
session_start();
include("config.php");
extract($_POST);
if (!empty($_POST)) {
    $emailId = $_POST['email_id'];
    $scannerSerialNo = $_POST['scanner_serial_no'];
    $companyName = $_POST['company_name'];
    $contact_name = $_POST['contact_name'];
    $street_addr = $_POST['street_address'];
    $addr_2 = $_POST['address_line2'];
    $city = $_POST['city'];
    $stateOrRegion = $_POST['state_or_region'];
    $country = $_POST['country'];
    $postalCode = $_POST['postal_code'];
    if ($_POST['updatesNotify'] == 1) {
        $updatesNotify = 1;
    } else {
        $updatesNotify = 0;
    }

    $insert_media = "INSERT INTO [ciotiswd].installation_details(email, scanner_serial_no, company_name, contact_name, street_address, address_line2, city, state_or_region, country, postal_code, published) VALUES ('$emailId', '$scannerSerialNo', '$companyName', '$contact_name', '$street_addr', '$addr_2', '$city', '$stateOrRegion', '$country', '$postalCode','$updatesNotify')";
    $insertMediaQuery = sqlsrv_query($conn, $insert_media);

    $rows_affected = sqlsrv_rows_affected($insertMediaQuery);
    if ($rows_affected === false) {
        die(print_r(sqlsrv_errors(), true));
    } elseif ($rows_affected == -1) {
        echo "No information available.<br />";
    } else {
        $_SESSION['media_success_msg'] = "Media details added successfully";
    }
    //print_r($_SESSION);exit;
    header("Location: index.php");
    header('X-Frame-Options: SAMEORIGIN');
    header("Content-Security-Policy: default-src 'self'");
}
?>
<body>
    <?php include 'header.php'; ?>
    <div class="cover">
        <div class="cover-image" style="background-image : url('images/data.png'); background-color:rgb(25,45,105);"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center" style="text-shadow: 0 2px 8px #000;">
                    <h1 class="text-inverse"><?php echo $lang['heading1']; ?><br class="hidden-sm hidden-xs"><?php echo $lang['heading2']; ?></h1>
                    <p class="text-inverse"><?php echo $lang['heading3']; ?></p>
                    <br>
                </div>
            </div>
            <div class="row" style="margin-bottom:70px;">
                <div class="col-md-offset-2 col-md-8">
                    <form class="row" id="mediaForm" action="media.php" method="post">
                        <div class="panel panel-primary" id="formReq">
                            <div class="panel-body ">
                                <div class="form-group col-sm-6">
                                    <label for="inputEmail3" class="control-label"><?php echo $lang['email_optional']; ?></label>
                                    <input type="text" class="form-control email_id" id="email_id" name="email_id" placeholder="<?php echo $lang['placeholder1']; ?>">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="inputSerial" class="control-label"><?php echo $lang['Scanner_Serial_Number']; ?></label>
                                    <button tabindex="-1" id="popo" title="<?php echo $lang['tooltip_title']; ?>" data-container="body" data-content="<?php echo $lang['tooltip_content']; ?>" type="button" data-trigger="focus" data-toggle="popover" class="btn btn-xs btn-link" data-placement="right"><?php echo $lang['findit']; ?>
                                        <i class="fa fa-fw fa-question-circle"></i>
                                    </button>
                                    <input required type="text" class="form-control scanner_serial_no" id="scanner_serial_no" name="scanner_serial_no" placeholder="12345678">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="inputName" class="control-label"><?php echo $lang['companyname']; ?></label>
                                    <input type="text" class="form-control company_name" id="company_name" name="company_name" placeholder="<?php echo $lang['placeholder2']; ?>">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="inputName" class="control-label"><?php echo $lang['contactname']; ?></label>
                                    <input type="text" class="form-control contact_name" id="contact_name" name="contact_name" placeholder="<?php echo $lang['placeholder3']; ?>">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="inputName" class="control-label"><?php echo $lang['streetaddress']; ?></label>
                                    <input type="text" class="form-control street_address" id="street_address" name="street_address" placeholder="<?php echo $lang['placeholder4']; ?>">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="inputName" class="control-label"><?php echo $lang['addressline2']; ?></label>
                                    <input type="text" class="form-control address_line2" id="address_line2" name="address_line2"placeholder="<?php echo $lang['placeholder5']; ?>">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="inputName" class="control-label"><?php echo $lang['city']; ?></label>
                                    <input type="text" class="form-control city" id="city" name="city" placeholder="">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="inputName" class="control-label"><?php echo $lang['state_province_region']; ?></label>
                                    <input type="text" class="form-control state_or_region" id="state_or_region" name="state_or_region" placeholder="">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="inputName" class="control-label"><?php echo $lang['country']; ?></label>
                                    <input type="text" class="form-control country" id="country" name="country" placeholder="">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="inputName" class="control-label"><?php echo $lang['postalcode']; ?></label>
                                    <input type="text" class="form-control postal_code" id="postal_code" name="postal_code" placeholder="">
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" value='1' class="btn btn-block btn-lg btn-primary" id="btnMed"  disabled="disabled" style="margin-bottom: 15px;"><?php echo $lang['send_me_disk']; ?></button>
                                </div>

                                <div class="phy_disk"><a href="index.php"><i class="fa fa-fw fa-arrow-circle-left"></i><?php echo $lang['prefer']; ?></a></div>
                            </div>
                            <div class="panel-footer">
                                <label>
                                    <input type="checkbox" name="updatesNotify" value="1" checked="" class="check_pos"><span class="p_notify"><?php echo $lang['note']; ?></span>
                                </label>
                                <div>
                                    <i class="fa fa-fw fa-lock"></i><?php echo $lang['policy1']; ?>
                                    <a href="http://www.kodak.com/go/newprivacy" target="_blank"><?php echo $lang['policy2']; ?></a>.
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>

