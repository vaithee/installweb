<?php

include("config.php");

session_start();
if (isset($_GET['reSendEmailId']) && !empty($_GET['reSendEmailId'])) {  ///////////// re-send email link
    $email_id = $_GET['reSendEmailId'];
    if (isset($_SESSION['email_id'])) {
        $_SESSION["email_id"] = $email_id;
    }
    $productId = $_GET['p_id'];
    $updatesNotify = 0;
    $selectedLang = 'en-US';
    $get_serial_no = "SELECT serial_no FROM [ciotiswd].product_details where id = '" . $productId . "'";
    $getSerialNo = sqlsrv_query($conn, $get_serial_no);
    $serial_no = array();
    while ($row = sqlsrv_fetch_array($getSerialNo)) {
        $serial_no = $row['serial_no'];
    }
//    echo $email_id . "////" . $selectedLang . "///" . $updatesNotify . '///' . $productId;
} else {  ///////////// firt time login
    $email_id = $_POST['inputEmail'];
    $_SESSION["email_id"] = $email_id;
    $serial_no = $_POST['inputSerial'];

    $get_product_id = "SELECT id FROM [ciotiswd].product_details where serial_no = '" . $serial_no . "'";
    $stmt = sqlsrv_query($conn, $get_product_id);

    $product_id = array();
    while ($row = sqlsrv_fetch_array($stmt)) {
        $productId = $row['id'];
    }
    if ($_POST['updatesNotify'] == 1) {
        $updatesNotify = 1;
    } else {
        $updatesNotify = 0;
    }
    if (isset($_SESSION['lang'])) {
        $selectedLang = $_SESSION['lang'];
    }
}

$browser = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/Linux/', $browser)) {
    $os = 'Linux';
} elseif (preg_match('/Win/', $browser)) {
    $os = 'Windows';
} elseif (preg_match('/Mac/', $browser)) {
    $os = 'Mac';
} else {
    $os = 'UnKnown';
}

//echo $email_id . "////" . $selectedLang . "///" . $updatesNotify . '///' . $productId;

if (isset($productId) && ($productId != null)) {
    $insert_models = "INSERT INTO [ciotiswd].models(product_id, operating_system, driver_type, user_language) VALUES ('$productId', '$os', 'softcopy', '$selectedLang')";
    $insertModelQuery = sqlsrv_query($conn, $insert_models);

    $last_model_id = "SELECT MAX(id) AS LastID FROM [ciotiswd].models";
    $selectLastModelId = sqlsrv_query($conn, $last_model_id);
    while ($row = sqlsrv_fetch_array($selectLastModelId)) {
        $modelId = $row[0];
    }
    $insert_customers = "INSERT INTO [ciotiswd].customers(product_id, model_id, email_id, serial_no, published) VALUES ($productId, $modelId, '$email_id', '$serial_no', $updatesNotify)";
    $customerQuery = sqlsrv_query($conn, $insert_customers);
    $_SESSION['product_id'] = $productId;    //to send product id to email
    $_SESSION['email_id'] = $email_id;
    include("emailConfig.php");

    $status = true;
    header("Location: index.php?status=success");
} else {
    $status = false;
}
echo json_encode($status);
?>