<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define('CH_TEST', 0); // possible value 0,1
define("CH_DEBUG", 1);

function ch_debug($var, $exit = false, $configable = true) {
    $templates[] = <<<EOT
        <div style='text-align:left;border-top:1px solid #ccc;background-color:white;color:black;overflow:auto;' >
            <pre>
                <br /> <strong> line : </strong> {line}
                <br /> <strong> file : </strong> {file}
                <br /> {data}
            </pre>
        </div>
EOT;

    $templates[] = <<<EOT
        \n
        -------------------------------debug ---------------------------
        line : {line}, file : {file}
        output: ->
        {data}
        ----------------------------------------------------------------
        \n\r
EOT;

    if (CH_DEBUG || $exit == true) {
        $debug_traces = debug_backtrace();
        $debug_trace = $debug_traces[0];

        $str = strtr($templates[CH_TEST], array("{line}" => "{$debug_trace['line']}",
            "{file}" => "{$debug_trace['file']}",
            "{data}" => print_r($var, true) //debug_trace['args'][0]
        ));
        echo $str;
        if ($exit == true)
            exit;
    }
}
