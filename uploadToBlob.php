<?php

$containerName = strtolower(str_replace(" ", "-", trim($_GET['prodSeries'])));

require_once 'WindowsAzure/WindowsAzure.php';
require_once 'HTTP/Request2.php';

use WindowsAzure\Blob\Models\BlobBlockType;
use WindowsAzure\Blob\Models\Block;
use WindowsAzure\Blob\Models\CommitBlobBlocksOptions;
use WindowsAzure\Blob\Models\CreateBlobOptions;
use WindowsAzure\Blob\Models\CreateContainerOptions;
use WindowsAzure\Blob\Models\ListContainersOptions;
use WindowsAzure\Blob\Models\PublicAccessType;
use WindowsAzure\Common\Internal\IServiceFilter;
use windowsazure\common\Internal\Resources;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\Common\CloudConfigurationManager;

$connectionString = 'DefaultEndpointsProtocol=https;AccountName=iminteractiveinstall2199;AccountKey=77uYvNagWxCt5V2JTsk+FCAoADdx+O1NoNrHLPXdX8LQjr9mmj+8tiAQ33bONA304QcRT8OEtZun1oaqC0BEYw==';
//$blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);

error_reporting(E_ALL);
ini_set('display_errors', 1);


$bytesInPostRequestBody = strlen(file_get_contents('php://input'));
ini_set('upload_max_filesize', '2048M');
//echo ini_get('upload_max_filesize'), ", ", ini_get('post_max_size');
print_r($_FILES['file']['tmp_name']);
print_r($_FILES['file']['name']);
print_r(str_replace(" ", "-", $_FILES['file']['name']));
//$uploaded_file = fopen($_FILES['file']['tmp_name'], "r");


define("CONTAINERNAME", $containerName);
define("BLOCKBLOBNAME", str_replace(" ", "-", $_FILES['file']['name']));
define("BLOCKSIZE", 4 * 1024 * 1024);    // Size of the block, modify if needed.
define("PADLENGTH", 5);                  // Size of the string used for the block ID, modify if needed.


define("FILENAME", $_FILES['file']['tmp_name']);
define('BLOB_GUESTBOOK', 'guestbook');

// Local file to upload as a block blob.
//-------------------------
function createContainerIfNotExists($blobRestProxy) {
    $listContainersOptions = new ListContainersOptions;
    $listContainersOptions->setPrefix(CONTAINERNAME);
    $listContainersResult = $blobRestProxy->listContainers($listContainersOptions);
    $containerExists = false;
    foreach ($listContainersResult->getContainers() as $container) {
        if ($container->getName() == CONTAINERNAME) {
            $containerExists = true;
            break;
        }
    }
    if (!$containerExists) {
        echo "Creating container.\n";
        $createContainerOptions = new CreateContainerOptions();
        $createContainerOptions->setPublicAccess(PublicAccessType::CONTAINER_AND_BLOBS);
        $blobRestProxy->createContainer(CONTAINERNAME, $createContainerOptions);
        echo "Container '" . CONTAINERNAME . "' successfully created.\n";
    }
}

try {
    //  $connectionString = CloudConfigurationManager::getConnectionString("$connectionString");
    //  $connectionString = "DefaultEndpointsProtocol=https;AccountName=yourAccount;AccountKey=yourKey";
    if (null == $connectionString || "" == $connectionString) {
        echo "Did not find a connection string whose name is 'StorageConnectionString'.";
        exit();
    }

    $blobRestProxy = ServicesBuilder::getInstance()->createBlobService($connectionString);
    createContainerIfNotExists($blobRestProxy);
//    echo "Using the '" . CONTAINERNAME . "' container and the '" . BLOCKBLOBNAME . "' blob.\n";
//    echo "Using file '" . FILENAME . "'\n";
    if (!file_exists(FILENAME)) {
        echo "The '" . FILENAME . "' file does not exist. Exiting program.\n";
        exit();
    }
    $handle = fopen(FILENAME, "r");
    $counter = 1;
    $blockIds = array();
    while (!feof($handle)) {
        $blockId = str_pad($counter, PADLENGTH, "0", STR_PAD_LEFT);

        $block = new Block();
        $block->setBlockId(base64_encode($blockId));
        $block->setType("Uncommitted");
        array_push($blockIds, $block);

        $data = fread($handle, BLOCKSIZE);

        // Upload the block.
        $blobRestProxy->createBlobBlock(CONTAINERNAME, BLOCKBLOBNAME, base64_encode($blockId), $data);
        $counter++;
    }
    // Done creating the blocks. Close the file and commit the blocks.
    fclose($handle);
    $blobRestProxy->commitBlobBlocks(CONTAINERNAME, BLOCKBLOBNAME, $blockIds);
} catch (ServiceException $serviceException) {
    $code = $serviceException->getCode();
    $error_message = $serviceException->getMessage();
} catch (Exception $exception) {
    $code = $exception->getCode();
    $error_message = $exception->getMessage();
}
?>