<?php

if (isSet($_POST['lang'])) {
    $lang = $_POST['lang'];
    $_SESSION['lang'] = $lang;
    //setcookie('lang', $lang, time() + (3600 * 24 * 30));
    setcookie('lang', $lang, time() + (3600 * 24 * 30), '/', $_SERVER['SERVER_NAME'], true, false);
} else if (isSet($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else if (isSet($_COOKIE['lang'])) {
    $lang = $_COOKIE['lang'];
} else {
    $lang = '?lang=en-US';
}

switch ($lang) {
    case '?lang=en-US':
        $email_template = 'languages/en/email_template_en.php';
        break;
    case '?lang=fr':
        $email_template = 'languages/fr/email_template_fr.php';
        break;
    case '?lang=ja':
        $email_template = 'languages/ja/email_template_ja.php';
        break;
    case '?lang=de':
        $email_template = 'languages/de/email_template_de.php';
        break;
    case '?lang=es':
        $email_template = 'languages/es/email_template_es.php';
        break;
    case '?lang=it':
        $email_template = 'languages/it/email_template_it.php';
        break;
    case '?lang=ko':
        $email_template = 'languages/ko/email_template_ko.php';
        break;
    case '?lang=pt-br':
        $email_template = 'languages/pt-br/email_template_pt-br.php';
        break;
    case '?lang=ru':
        $email_template = 'languages/ru/email_template_ru.php';
        break;
    case '?lang=zh-cn':
        $email_template = 'languages/zh-cn/email_template_zh-cn.php';
        break;
    case '?lang=zh-tw':
        $email_template = 'languages/zh-tw/email_template_zh-tw.php';
        break;
    default:
        $email_template = 'languages/en/email_template_en.php';
}
require_once('PHPMailer-master/class.phpmailer.php');
require_once('PHPMailer-master/class.smtp.php');
$BASE_PATH = parse_url(strtok("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", '?'));
$BASE_PATH = "http://" . $BASE_PATH["host"];

$mail = new PHPMailer(true);

$mail->isSMTP();
$mail->Host = 'smtp.office365.com';
$mail->SMTPAuth = true;
//$mail->Username = 'azure_d9eff6737b1e375cc56650f0be275a5f@azure.com'; 0829MargGar
$mail->Username = 'RInstallScannerSMTP@kodakalaris.com';
$mail->Password = 'SWdeliv1';
$mail->Port = 587;
$mail->SMTPSecure = 'tls';
$mail->IsSMTP();
$mail->SMTPDebug = 0;

try {

    $mail->AddAddress($email_id);
    $mail->SetFrom('RInstallScannerSMTP@kodakalaris.com', 'Kodak Alaris Scanner Software');
    $mail->Subject = 'Kodak Alaris - Software Download';
    $body = file_get_contents($email_template);
//    if (isset($_SESSION['product_id'])) {
//        echo "product id: " . $_SESSION['product_id'];
//    } else {
//        echo "no product id available";
//    }
    $startdate = date("Y-m-d:H:i:s");
    $enddate = date("Y-m-d:H:i:s", strtotime("+3 days"));
    $body = str_replace('$productId', urlencode(base64_encode($productId)), $body);
    $body = str_replace('$startdate', urlencode(base64_encode($startdate)), $body);
    $body = str_replace('$enddate', urlencode(base64_encode($enddate)), $body);
    $body = str_replace('$BASE_PATH', $BASE_PATH, $body);
    $body = str_replace('$emailId', urlencode(base64_encode($email_id)), $body);
    //$body = str_replace('$lang', urlencode(base64_encode($lang)), $body);
    //$body = str_replace('$lang', $lang, $body);
//    print_r($body);exit;
    $mail->MsgHTML($body);
    $mail->Send();
//    echo "Message Sent OK<p></p>\n";
} catch (phpmailerException $e) {
    //echo $e->errorMessage(); 
} catch (Exception $e) {
    // echo $e->getMessage(); 
}
?>