<!DOCTYPE html>
<head>
    <title>Kodak Alaris::Information Management</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/ico" href="images/favicon.ico">
    <link rel="icon" type="image/x-icon" href="images/favicon.ico">
    <link rel="icon" type="image/png" href="images/favicon.ico">
    <link href="css/pingendo-bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="css/KodakAlaris_IM_bootstrap.css">
    <link href="css/kodak_style.css" rel="stylesheet" type="text/css"/>
    <link href="css/fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="js/jquery-2.2.3.min.js" type="text/javascript"></script>
    <script src="js/jquery.validate.js" type="text/javascript"></script>
    <script src="js/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/custom.js" type="text/javascript"></script>
</head>
<script>
    $(document).ready(function () {
        var p1 = $("#para1").text();
        $("#p1").text(p1);
    });
</script>
<body>
    <?php
    if (isSet($_GET['lang'])) {
        $lang = $_GET['lang'];
        $_SESSION['lang'] = $lang;
        //setcookie('lang', $lang, time() + (3600 * 24 * 30));
        setcookie('lang', $lang, time() + (3600 * 24 * 30), '/', $_SERVER['SERVER_NAME'], true, false);
    } else if (isSet($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
    } else if (isSet($_COOKIE['lang'])) {
        $lang = $_COOKIE['lang'];
    } else {
        $lang = 'en';
    }

    switch ($lang) {
        case 'en':
            $lang_file = 'en/lang.en.php';
            break;
        case 'fr':
            $lang_file = 'fr/lang.fr.php';
            break;
        case 'ja':
            $lang_file = 'ja/lang.ja.php';
            break;
        case 'de':
            $lang_file = 'de/lang.de.php';
            break;
        case 'es':
            $lang_file = 'es/lang.es.php';
            break;
        case 'it':
            $lang_file = 'it/lang.it.php';
            break;
        case 'ko':
            $lang_file = 'ko/lang.ko.php';
            break;
        case 'pt-br':
            $lang_file = 'pt-br/lang.pt-br.php';
            break;
        case 'ru':
            $lang_file = 'ru/lang.ru.php';
            break;
        case 'zh-cn':
            $lang_file = 'zh-cn/lang.zh-cn.php';
            break;
        case 'zh-tw':
            $lang_file = 'zh-tw/lang.zh-tw.php';
            break;
        default:
            $lang_file = 'en/lang.en.php';
    }
    include_once 'languages/' . $lang_file;

    echo "<p id='para1'>" . $lang['para1'] . "</p>";
    ?>
</body>
</html>