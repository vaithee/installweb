<?php

session_start();
error_reporting(0);

if (isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    include 'config.php';

    if (isset($_REQUEST['search_criteria'])) {
        $search_critiya_checklist = $_REQUEST['search_criteria'];
    } else {
        $search_critiya_checklist = $_SESSION['search_criteria'];
    }

    if (isset($_REQUEST['product_name'])) {
        $product_name_checklist = $_REQUEST['product_name'];
    } else {
        $product_name_checklist = $_SESSION['product_name'];
    }


    if (isset($_REQUEST['category'])) {
        $category_checklist = $_REQUEST['category'];
    } else {
        $category_checklist = $_SESSION['category'];
    }

    if (isset($_REQUEST['daterange'])) {
        $daterange_checklist = $_REQUEST['daterange'];
    } else {
        $daterange_checklist = $_SESSION['daterange'];
    }

    $_SESSION['search_critiya_checklist'] = $search_critiya_checklist;
    $_SESSION['product_name_checklist'] = $product_name_checklist;
    $_SESSION['category_checklist'] = $category_checklist;
    $_SESSION['daterange_checklist'] = $daterange_checklist;
    if (empty($_SESSION['search_critiya_checklist']) && empty($_SESSION['product_name_checklist']) && empty($_SESSION['category_checklist']) && empty($_SESSION['daterange_checklist'])) {
        $data = array();
        $error = 0;
        $results_title = sqlsrv_query($conn, "SELECT DISTINCT a.product_family  FROM [ciotiswd].product_owner_list_content a INNER JOIN [ciotiswd].reports b ON a.friendly_name = b.filename INNER JOIN [ciotiswd].product_owner_list c ON c.product_name LIKE '%' + b.product_name + '%' where a.product_family!=''")or die(print_r(sqlsrv_errors(), true) . "q1");
        $count = 1;
        while ($data_title = sqlsrv_fetch_array($results_title)) {
            $error = 1;
            if ($count === 1) {

                $class = 'in';
            } else {

                $class = '';
            }
            $display = '';
            $display1 = '';
            $sqldownload = "SELECT SUM(download_count) AS download_count, SUM(eligiblescan) AS eligiblescan FROM [ciotiswd].reports a INNER JOIN [ciotiswd].product_owner_list_content b ON a.filename = b.friendly_name INNER JOIN [ciotiswd].product_owner_list c ON b.product_owner_list_id = c.id and c.product_name LIKE '%' + a.product_name + '%' where b.product_family='" . $data_title['product_family'] . "'" or die(print_r(sqlsrv_errors(), true) . "q2");
            $resultDownload = sqlsrv_query($conn, $sqldownload);
            $rowDownload = sqlsrv_fetch_array($resultDownload);
            $sum = $rowDownload['download_count'];
            $eligiblescan = $rowDownload['eligiblescan'];
            $attachrate = $sum / $eligiblescan * 100;
            echo ' <div class="row list">
                    <div class="col-sm-12">
                        <div class="row" role="button" data-toggle="collapse" href="#' . $count . '">
                            <div class="col-md-3">' . $data_title['product_family'] . '(All Versions)</div>
                            <div class="col-md-3 text-right req">' . number_format($eligiblescan) . '</div>
                            <div class="col-md-3 text-right">';
            echo '' . number_format($sum) .
            ' </div>
                            <div class="col-md-3 text-right">' . floor($attachrate) . '&#37;</div>
                        </div>								
                       <div class="row collapse hide_accordion ' . $class . '" id="' . $count . '">';
            $display = "
		    <table border='1' >
                        <thead>
                           <tr>
                              <th>Date Range</th>
                              <th>Search / filter</th>
                              <th>Type</th>
                              <th>Scanner Model</th>
                              <th>Title</th>
                              <th>Requests</th>
                              <th>Downloads</th>
                              <th>Attach Rate</th>  
                           </tr>
                        </thead>
                        <tbody>";
            $display1 = "
		              <tr>
                              <td>All</td>
                              <td>All</td>
                              <td>All</td>
                              <td>All</td>
                              <td>" . $data_title['product_family'] . "(All Versions)</td>
                              <td>" . $eligiblescan . "</td>
                              <td>" . number_format($sum) . "</td>
                              <td>" . floor($attachrate) . "&#37;</td></tr>";
            $results = sqlsrv_query($conn, "SELECT * FROM [ciotiswd].reports a INNER JOIN [ciotiswd].product_owner_list_content b ON a.filename = b.friendly_name INNER JOIN [ciotiswd].product_owner_list c ON b.product_owner_list_id = c.id and c.product_name LIKE '%' + a.product_name + '%'  where b.product_family='" . $data_title['product_family'] . "' ORDER BY b.product_family ASC")or die(print_r(sqlsrv_errors(), true) . "q3");
            $inner1 = '';
            while ($data = sqlsrv_fetch_array($results)) {
                $inner = '';
                echo '
            <div class="col-sm-12 search">
                <div class="row"  style="padding:2px 0px;">
                        <div class="col-md-3" style="font-size:14px;">' . $data['friendly_name'] . " " . $data['version'] . '</div>
                        <div class="col-md-3 text-right" style="font-size:14px;">' . number_format($data['eligiblescan']) . '</div>
                        <div class="col-md-3 text-right" style="font-size:14px;">' . number_format($data['download_count']) . '</div>
                        <div class="col-md-3 text-right" style="font-size:14px;">' . $data['attachrate'] . '&#37;</div>
                </div>
            </div>';
                $inner = "  <tr >
                  <td>All</td>
                  <td>All</td>
                  <td>All</td>
                  <td>All</td>
                  <td>" . $data['friendly_name'] . " " . $data['version'] . "</td>
                  <td>" . number_format($data['eligiblescan']) . "</td>
                  <td>" . number_format($data['download_count']) . "</td>
                  <td>" . $data['attachrate'] . "&#37;</td>
                  </tr>";
                $inner1.=$inner;
            }
            echo '</div>
           </div>
           </div>';
            $end = "</tbody></table>";
            $count++;
            $value_tbl.=$display1 . $inner1;
        }
        $_SESSION['export'] = '<div id="dvData">' . $display . $value_tbl . $end . '</div>';
        echo '<div style="display:none">' . $_SESSION['export'] . '</div>';
        if ($error == 0) {
            $no_record = "no_record";
            echo '<span class=' . $no_record . '>NO RECORDS FOUND</span>';
        }
    } else {
        unset($_SESSION['export']);
        $WHERE = array();
        $inner = $w = '';
        $WHERE1 = array();
        $w1 = '';
        $_SESSION['search_critiya_checklist'] = $search_critiya_checklist;
        $_SESSION['product_name_checklist'] = $product_name_checklist;
        $_SESSION['category_checklist'] = $category_checklist;
        $_SESSION['daterange_checklist'] = $daterange_checklist;
        $dateformat = str_replace("-", "to", $_SESSION['daterange_checklist']);
        $dateformat1 = str_replace("/", "-", $dateformat);
        $order_list = explode(" to ", $dateformat1);
        $from = $order_list[0];
        $to = $order_list[1];
        $pro_name = sqlsrv_query($conn, "SELECT * FROM [ciotiswd].product_owner_list where product_name='" . $_SESSION['product_name_checklist'] . "'") or die(print_r(sqlsrv_errors(), true) . "q4");
        $pro_name_title = sqlsrv_fetch_array($pro_name);
        $pro_name_date = sqlsrv_query($conn, "SELECT * FROM [ciotiswd].reports where date between '" . $from . "' and '" . $to . "'") or die(print_r(sqlsrv_errors(), true) . "q5");
        $date_wise_fn = '';
        while ($pro_name_title_date = sqlsrv_fetch_array($pro_name_date)) {
            $date_wise_fn.="'" . $pro_name_title_date['filename'] . "',";
        }
        $date_wise_fn = rtrim($date_wise_fn, ',');
        $date_wise_fn = $date_wise_fn;
        $search = 'All';
        if (!empty($_SESSION['search_critiya_checklist'])) {
            $WHERE[] = "(b.friendly_name LIKE  '" . $_SESSION['search_critiya_checklist'] . "%' or b.version LIKE '" . $_SESSION['search_critiya_checklist'] . "%') ";
            $WHERE1[] = "(a.friendly_name LIKE '" . $_SESSION['search_critiya_checklist'] . "%' OR a.version LIKE '" . $_SESSION['search_critiya_checklist'] . "%') ";

            $search = $_SESSION['search_critiya_checklist'];
        }
        $pr_name = 'All';
        if (!empty($_SESSION['product_name_checklist'])) {
            $WHERE[] = "(c.product_name = '" . $_SESSION['product_name_checklist'] . "')";
            $WHERE1[] = "(a.product_owner_list_id = '" . $pro_name_title['id'] . "')";

            $pr_name = $_SESSION['product_name_checklist'];
        }
        $der = 'All';
        if (!empty($_SESSION['category_checklist'])) {
            $WHERE[] = "(b.category = '" . $_SESSION['category_checklist'] . "') ";
            $WHERE1[] = "(category = '" . $_SESSION['category_checklist'] . "') ";
            $der = $_SESSION['category_checklist'];
        }
        $daterange = 'All';
        if (!empty($from) && !empty($to)) {
            $WHERE[] = "(date between '" . $from . "' and '" . $to . "')";

            if (!empty($date_wise_fn)) {
                $WHERE1[] = "b.filename IN (" . $date_wise_fn . ") ";
            } else {
                $WHERE1[] = "b.filename=''";
            }

            $daterange = $_SESSION['daterange_checklist'];
        }
        $w = implode(' AND ', $WHERE);
        $w1 = implode(' AND ', $WHERE1);
        if (!empty($w))
            $w = $w;
        if (!empty($w1))
            $w1 = 'and ' . $w1;
        $data = array();
        $error = 0;
        $results_title = sqlsrv_query($conn, "SELECT DISTINCT a.product_family  FROM [ciotiswd].product_owner_list_content a INNER JOIN [ciotiswd].reports b ON a.friendly_name = b.filename INNER JOIN [ciotiswd].product_owner_list c ON c.product_name LIKE '%' + b.product_name + '%' where a.product_family!='' " . $w1 . " ")or die(print_r(sqlsrv_errors(), true) . "q6");
        $count = 1;
        while ($data_title = sqlsrv_fetch_array($results_title)) {
            $error = 1;
            if ($count === 1) {

                $class = 'in';
            } else {

                $class = '';
            }
            $results_title_find = sqlsrv_query($conn, "SELECT * FROM [ciotiswd].product_owner_list_content where id='" . $data_title['id'] . "'")or die(print_r(sqlsrv_errors(), true) . "q7");
            $data_title_find = sqlsrv_fetch_array($results_title_find);
            //echo"SELECT * FROM product_owner_list_content where product_family!='' ".$w1."  GROUP BY product_family ".$data_title_find['product_family']."-".$_SESSION['product_name_checklist']."";
            if ((!empty($_SESSION['search_critiya_checklist']) && strpos($data_title_find['friendly_name'], $_SESSION['search_critiya_checklist']) == false) ||
                    (!empty($_SESSION['category_checklist']) && strpos($data_title_find['category'], $_SESSION['category_checklist']) == false) ||
                    (!empty($_SESSION['product_name_checklist']) && strpos($data_title_find['product_family'], $_SESSION['product_name_checklist']) == false) ||
                    (!empty($date_wise_fn) && strpos($date_wise_fn, $data_title_find['product_family']) == false)) {
                
            }
            $display = '';
            $display1 = '';
            $sqldownload = "SELECT SUM(download_count) AS download_count, SUM(eligiblescan) AS eligiblescan FROM [ciotiswd].reports a INNER JOIN [ciotiswd].product_owner_list_content b ON a.filename = b.friendly_name INNER JOIN [ciotiswd].product_owner_list c ON b.product_owner_list_id = c.id and c.product_name LIKE '%' + a.product_name + '%' where b.product_family='" . $data_title['product_family'] . "'" or die(print_r(sqlsrv_errors(), true) . "q8");
            $resultDownload = sqlsrv_query($conn, $sqldownload);
            $rowDownload = sqlsrv_fetch_array($resultDownload);
            $sum = $rowDownload['download_count'];
            $eligiblescan = $rowDownload['eligiblescan'];
            $attachrate = $sum / $eligiblescan * 100;
            echo ' <div class="row list">
                    <div class="col-sm-12">
                        <div class="row" role="button" data-toggle="collapse" href="#' . $count . '">
                            <div class="col-md-3">' . $data_title['product_family'] . '(All Versions)</div>
                            <div class="col-md-3 text-right request">' . number_format($eligiblescan) . '</div>
                            <div class="col-md-3 text-right">';
            echo '' . number_format($sum) . ' </div>
                            <div class="col-md-3 text-right">' . floor($attachrate) . '&#37;</div>
                        </div>			
                       <div class="row collapse hide_accordion ' . $class . '" id="' . $count . '">';
            $display = "
			  <table border='1' >
                        <thead>
                           <tr style='font-size:20px;color:#000:font-weight:bold;'>
                              <th>Date Range</th>
                              <th>Search / filter</th>
                              <th>Type</th>
                              <th>Scanner Model</th>
                              <th>Title</th>
                              <th>Requests</th>
                              <th>Downloads</th>
                              <th>Attach Rate</th>  
                           </tr>
                        </thead>
                        <tbody>";
            $display1 = "
			      <tr>
                              <td>" . $daterange . "</td>
                              <td>" . $search . "</td>
                              <td>" . $der . "</td>
                              <td>" . $pr_name . "</td>
                              <td>" . $data_title['product_family'] . "(All Versions)</td>
                              <td>" . $eligiblescan . "</td>
                              <td>" . number_format($sum) . "</td>
                              <td>" . floor($attachrate) . "&#37;</td></tr>";
            $results = sqlsrv_query($conn, "SELECT * FROM [ciotiswd].reports a INNER JOIN [ciotiswd].product_owner_list_content b ON a.filename = b.friendly_name INNER JOIN [ciotiswd].product_owner_list c ON b.product_owner_list_id = c.id and c.product_name LIKE '%' + a.product_name + '%' WHERE b.product_family='" . $data_title['product_family'] . "' and " . $w . " ")or die(print_r(sqlsrv_errors(), true) . "q9");
            $inner1 = '';
            while ($data = sqlsrv_fetch_array($results)) {
                $inner = '';
                echo '
            <div class="col-sm-12 search">
                <div class="row"  style="padding:2px 0px;">
                    <tr>
                        <td><div class="col-md-3" style="font-size:14px;">' . $data['friendly_name'] . " " . $data['version'] . '</div></td>
                        <td><div class="col-md-3 text-right" style="font-size:14px;">' . number_format($data['eligiblescan']) . '</div></td>
                        <td><div class="col-md-3 text-right" style="font-size:14px;">' . number_format($data['download_count']) . '</div></td>
                        <td><div class="col-md-3 text-right" style="font-size:14px;">' . $data['attachrate'] . '&#37;</div></td>
                    </tr>
                </div>
            </div>';
                $inner = " <tr>
                  <td>" . $daterange . "</td>
                  <td>" . $search . "</td>
                  <td>" . $der . "</td>
                  <td>" . $pr_name . "</td>
                  <td>" . $data['friendly_name'] . " " . $data['version'] . "</td>
                  <td>" . number_format($data['eligiblescan']) . "</td>
                  <td>" . number_format($data['download_count']) . "</td>
                  <td>" . $data['attachrate'] . "&#37;</td>
                  </tr> ";
                $inner1.=$inner;
            }
            echo '</div>
           </div>
           </div>';
            $end = "</tbody></table>";
            $count++;
            $value_tbl.=$display1 . $inner1;
        }
        unset($_SESSION['export']);
        $_SESSION['export'] = '<div id="dvData">' . $display . $value_tbl . $end . '</div>';
        echo '<div style="display:none">' . $_SESSION['export'] . '</div>';
    }
    if ($error == 0) {
        $no_record1 = "no_record1";
        echo '<span class = ' . $no_record1 . '>NO RECORDS FOUND</span>';
    }
}
?>
