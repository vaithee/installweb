<?php

$lang = array();

$lang['head_lbl_1'] = "讓我們來安裝您的全新";
$lang['head_lbl_2'] = "Kodak Alaris 掃描器.";
$lang['head_cap'] = "我們只需要您的電子郵件地址和掃描器序號。.";
$lang['email'] = "電子郵件";
$lang['scanner_serial_no'] = "掃描器序號";
$lang['find_it'] = "尋找它";
$lang['download_link'] = "傳送連結給我";
//$lang['physical_disk'] = "Besoin d'un disque physique?";
$lang['notify'] = "我們只會將您的電子郵件地址用於履行您的要求";
$lang['privacy_policy_1'] = "我們重視您的隱私權。.";
$lang['privacy_policy_2'] = "檢視我們的隱私權政策";
$lang['placeholder_email'] = "my.name@example.com";
$lang['tooltip_title'] = "尋找您的序號";
$lang['tooltip_content'] = "在掃描器背面或底部即可找到您的序號標籤。.";
$lang['emailCheck'] = "檢查您的電子郵件";
$lang['sent_link'] = "我們已傳送連結至";
$lang['spam_mail'] = "如果您在 10 分鐘內沒收到信，請檢查您的垃圾郵件或待過濾郵件資料夾。.";
$lang['visit1'] = "還是沒收到信？";
$lang['visit2'] = "請造訪我們的支援中心。.";
?>