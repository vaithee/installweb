<?php

$lang = array();

$lang['head_lbl_1'] = "Installieren Sie Ihren neuen";
$lang['head_lbl_2'] = "Kodak Alaris Scanner.";
$lang['head_cap'] = "Wir benötigen lediglich Ihre E-Mail-Adresse und Scanner-Seriennummer.";
$lang['email'] = "E-Mail-Adresse";
$lang['scanner_serial_no'] = "Scanner-Seriennummer";
$lang['find_it'] = "Suchen";
$lang['download_link'] = "Link senden";
//$lang['physical_disk'] = "Besoin d'un disque physique?";
$lang['notify'] = "Wir verwenden Ihre E-Mail-Adresse lediglich zur Bearbeitung Ihrer Anfrage.";
$lang['privacy_policy_1'] = "Wir legen Wert auf den Schutz Ihrer Daten.";
$lang['privacy_policy_2'] = "Lesen Sie unsere Datenschutzrichtlinie.";
$lang['placeholder_email'] = "mein.Name@Beispiel.com";
$lang['tooltip_title'] = "Suchen Sie nach Ihrer Seriennummer.";
$lang['tooltip_content'] = "Das Etikett mit Ihrer Seriennummer befindet sich auf der Rück- oder Unterseite Ihres Scanners.";
$lang['model_Number'] = "Modellnummer";
$lang['emailCheck'] = "Prüfen Sie Ihre E-Mails.";
$lang['sent_link'] = "Wir haben einen Link an folgende E-Mail-Adresse gesendet:";
$lang['spam_mail'] = "Wenn Sie die E-Mail nach 10 Minuten immer noch nicht erhalten haben, sehen Sie in Ihrem Spam-, Clutter- oder Junk-Ordner nach.";
$lang['visit1'] = "Haben Sie sie noch nicht erhalten?";
$lang['visit2'] = "Besuchen Sie die Website unseres Support Center.";


/* media page */
$lang['heading1'] = "Lassen Sie sich die physischen Medien";
$lang['heading2'] = "für Ihren Kodak Alaris Scanner zusenden.";
$lang['heading3'] = "Teilen Sie uns einfach Ihre Adresse mit.";
$lang['email_optional'] = "E-Mail-Adresse (optional)";
$lang['Scanner_Serial_Number'] = "Scanner-Seriennummer";
$lang['findit'] = "Suchen";
//$lang['tooltip_title'] = "Suchen Sie nach Ihrer Seriennummer."; 
//$lang['tooltip_content'] = "Das Etikett mit Ihrer Seriennummer befindet sich auf der Rück- oder Unterseite Ihres Scanners."; 
$lang['companyname'] = "Firmenname";
$lang['placeholder1'] = "mein.Name@Beispiel.com";
$lang['placeholder2'] = "Beispiel GmbH";
$lang['contactname'] = "Name des Ansprechpartners";
$lang['placeholder3'] = "Vor- und Nachname";
$lang['streetaddress'] = "Straße, Nr.";
$lang['placeholder4'] = "Hauptstraße 123";
$lang['addressline2'] = "Adresszeile 2 (optional)";
$lang['placeholder5'] = "Zusatz";
$lang['city'] = "Ort";
$lang['state_province_region'] = "Bundesland";
$lang['country'] = "Land";
$lang['postalcode'] = "Postleitzahl";
$lang['send_me_disk'] = "Disk senden";
$lang['prefer'] = "Möchten Sie das Installationsprogramm herunterladen?";
$lang['note'] = "Mich über Software-Updates und besondere Angebote benachrichtigen.";
$lang['policy1'] = "Wir legen Wert auf den Schutz Ihrer Daten.";
$lang['policy2'] = "Lesen Sie unsere Datenschutzrichtlinie.";

/* Download Page */
$lang['choose_software'] = "Wählen Sie die Software, die heruntergeladen werden soll.";
$lang['download'] = "Herunterladen";
$lang['link_expired'] = "Ihr Link ist abgelaufen.";
$lang['expire_days'] = "Die Links laufen in 3 Tagen ab.";
$lang['new_link'] = "Wir können Ihnen für eine erneute Anfrage einen";
$lang['request_again'] = "neuen Link senden.";
$lang['send_link'] = "Neuen Link senden";
$lang['or'] = "oder";
$lang['different_email'] = "neue E-Mail senden";

/* Email Template */
$lang['para1'] = "Anbei senden wir Ihnen den Link zu Ihrer Kodak Alaris Scannersoftware.";
$lang['para2'] = "Anbei senden wir Ihnen Ihre Software von Kodak Alaris.";
$lang['para3'] = "Profitieren Sie von Ihren Bildern und Daten.";
$lang['para4'] = "Folgen Sie dem Link und laden Sie Ihre Software herunter, um die Installation Ihres Scanners abzuschließen. Der Link läuft in 3 Tagen ab.";
$lang['para5'] = "Software abrufen";
$lang['para6'] = "Kodak Alaris Inc. − Information Management, 2400 Mount Read Blvd. Rochester, NY 14615";
$lang['para7'] = "© 2016 Kodak Alaris Inc.";
$lang['para8'] = "Die Marke Kodak und das dazugehörige Design werden unter Lizenz der Eastman Kodak Company verwendet.";
?>