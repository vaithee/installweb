<?php

$lang = array();

$lang['head_lbl_1'] = "Давайте установим ваш новый";
$lang['head_lbl_2'] = "сканер Kodak Alaris.";
$lang['head_cap'] = "Вам потребуется только ваш адрес электронной почты и серийный номер сканера.";
$lang['email'] = "Адрес эл. почты";
$lang['scanner_serial_no'] = "Серийный номер сканера";
$lang['find_it'] = "Найдите его";
$lang['download_link'] = "Отправьте мне ссылку";
//$lang['physical_disk'] = "Besoin d'un disque physique?";
$lang['notify'] = "Мы воспользуемся вашим адресом электронной почты только для выполнения вашего запроса";
$lang['privacy_policy_1'] = "Мы ценим вашу конфиденциальность.";
$lang['privacy_policy_2'] = "Обзор нашей политики конфиденциальности.";
$lang['placeholder_email'] = "my.name@example.com";
$lang['tooltip_title'] = "Найдите серийный номер вашего сканера.";
$lang['tooltip_content'] = "Этикета с серийным номером находится на задней или нижней панели сканера.";
$lang['model_Number'] = "Номер модели";
$lang['emailCheck'] = "Проверьте электронную почту";
$lang['sent_link'] = "Мы отправили ссылку по адресу";
$lang['spam_mail'] = "Если вы не получите ее в течение 10 минут, проверьте папку нежелательных сообщений, рассылок и корзину.";
$lang['visit1'] = "Сообщение так и не было получено?";
$lang['visit2'] = "Посетите наш Центр поддержки.";


/* media page */
$lang['heading1'] = "Мы можем отправить физический носитель";
$lang['heading2'] = "для вашего сканера Kodak Alaris";
$lang['heading3'] = "Просто сообщите нам, куда его отправить.";
$lang['email_optional'] = "Адрес эл. почты (необязательно)";
$lang['Scanner_Serial_Number'] = "Серийный номер сканера";
$lang['findit'] = "Найдите его";
//$lang['tooltip_title'] = "Suchen Sie nach Ihrer Seriennummer."; 
//$lang['tooltip_content'] = "Das Etikett mit Ihrer Seriennummer befindet sich auf der Rück- oder Unterseite Ihres Scanners."; 
$lang['companyname'] = "Название компании";
$lang['placeholder1'] = "my.name@example.com";
$lang['placeholder2'] = "Example, Inc.";
$lang['contactname'] = "Контактное лицо";
$lang['placeholder3'] = "Имя Фамилия";
$lang['streetaddress'] = "Улица, дом";
$lang['placeholder4'] = "123 Мейн стрит";
$lang['addressline2'] = "Строка адреса 2 (необязательно)";
$lang['placeholder5'] = "Квартира, корпус, здание, строение";
$lang['city'] = "Город";
$lang['state_province_region'] = "Штат/провинция/область";
$lang['country'] = "Страна";
$lang['postalcode'] = "Почтовый индекс";
$lang['send_me_disk'] = "Отправьте мне диск";
$lang['prefer'] = "Предпочитаете загрузить установщик?";
$lang['note'] = "Уведомлять меня об обновлениях программного обеспечения и специальных предложениях.";
$lang['policy1'] = "Мы ценим вашу конфиденциальность.";
$lang['policy2'] = "Обзор нашей политики конфиденциальности.";


/* Download Page */
$lang['choose_software'] = "Выберите программное обеспечение для загрузки";
$lang['download'] = "Загрузить";
$lang['link_expired'] = "Срок действия ссылки истек";
$lang['expire_days'] = "Срок действия ссылок — 3 дня.";
$lang['new_link'] = "Не беспокойтесь. По вашему запросу мы отправим вам";
$lang['request_again'] = "новую ссылку";
$lang['send_link'] = "Отправьте мне новую ссылку";
$lang['or'] = "или";
$lang['different_email'] = "запросите другой адрес электронной почты";
?>