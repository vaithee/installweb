<?php

$lang = array();

$lang['head_lbl_1'] = "Vamos instalar seu novo";
$lang['head_lbl_2'] = "scanner Kodak Alaris.";
$lang['head_cap'] = "Tudo o que precisamos é o seu e-mail e número de série do scanner.";
$lang['email'] = "E-mail";
$lang['scanner_serial_no'] = "Número de série do scanner";
$lang['find_it'] = "Achar";
$lang['download_link'] = "Envie-me um link";
//$lang['physical_disk'] = "Besoin d'un disque physique?";
$lang['notify'] = "Utilizamos seu e-mail apenas para satisfazer o seu pedido";
$lang['privacy_policy_1'] = "Valorizamos a sua privacidade.";
$lang['privacy_policy_2'] = "Reveja a nossa política de privacidade.";
$lang['placeholder_email'] = "meu.nome@exemplo.com";
$lang['tooltip_title'] = "Ache o se número de série.";
$lang['tooltip_content'] = "Encontre a sua etiqueta do número de série na parte traseira ou inferior do scanner.";
$lang['model_Number'] = "Número de modelo";
$lang['emailCheck'] = "Verifique seu e-mail";
$lang['sent_link'] = "Enviamos um link para";
$lang['spam_mail'] = "Se você não vê-lo dentro de 10 minutos, verifique sua caixa de spam, lixo ou pasta de lixo eletrônico.";
$lang['visit1'] = "Ainda não recebeu?";
$lang['visit2'] = "Visite o nosso Centro de Suporte.";


/* media page */
$lang['heading1'] = "Obter mídia física que foi enviada";
$lang['heading2'] = "para o seuscanner Kodak Alaris";
$lang['heading3'] = "Diga-nos onde enviá-la.";
$lang['email_optional'] = "E-mail (opcional)";
$lang['Scanner_Serial_Number'] = "Número de série do scanner";
$lang['findit'] = "Achar";
//$lang['tooltip_title'] = "Suchen Sie nach Ihrer Seriennummer."; 
//$lang['tooltip_content'] = "Das Etikett mit Ihrer Seriennummer befindet sich auf der Rück- oder Unterseite Ihres Scanners."; 
$lang['companyname'] = "Nome da empresa";
$lang['placeholder1'] = "meu.nome@exemplo.com";
$lang['placeholder2'] = "Exemplo, Inc.";
$lang['contactname'] = "Nome do contato";
$lang['placeholder3'] = "Nome";
$lang['streetaddress'] = "Endereço";
$lang['placeholder4'] = "123 Rua Carlos Lins";
$lang['addressline2'] = "Linha do endereço 2 (opcional)";
$lang['placeholder5'] = "Apt, suite, unidade, prédio (opcional)";
$lang['city'] = "Cidade";
$lang['state_province_region'] = "Estado/Província/Região";
$lang['country'] = "País";
$lang['postalcode'] = "CEP";
$lang['send_me_disk'] = "Envie-me um disco";
$lang['prefer'] = "Prefere fazer download do instalador?";
$lang['note'] = "Notifique-me sobre atualizações de software e ofertas especiais.";
$lang['policy1'] = "Valorizamos a sua privacidade.";
$lang['policy2'] = "Reveja a nossa política de privacidade.";

/* Download Page */
$lang['choose_software'] = "Escolha o software que você deseja baixar";
$lang['download'] = "Download";
$lang['link_expired'] = "Seu link expirou";
$lang['expire_days'] = "Links expiram após 3 dias.";
$lang['new_link'] = "Não se preocupe. Podemos enviar um novo link para";
$lang['request_again'] = "solicitar novamente.";
$lang['send_link'] = "Envie-me um novo link";
$lang['or'] = "ou";
$lang['different_email'] = "solicite um e-mail diferente";
?>