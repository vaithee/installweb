<?php

$lang = array();

$lang['head_lbl_1'] = "Comencemos con la instalación de";
$lang['head_lbl_2'] = "su nuevo escáner Kodak Alaris.";
$lang['head_cap'] = "Lo único que necesitamos es su dirección de correo electrónico y el número de serie del escáner.";
$lang['email'] = "Correo electrónico";
$lang['scanner_serial_no'] = "Número de serie del escáner";
$lang['find_it'] = "Buscar";
$lang['download_link'] = "Deseo recibir un enlace";
//$lang['physical_disk'] = "Besoin d'un disque physique?";
$lang['notify'] = "Utilizamos su dirección de correo electrónico únicamente para completar su pedido";
$lang['privacy_policy_1'] = "Valoramos su privacidad.";
$lang['privacy_policy_2'] = "Revise nuestra política de privacidad.";
$lang['placeholder_email'] = "mi.nombre@ejemplo.com";
$lang['tooltip_title'] = "Buscar el número de serie del escáner.";
$lang['tooltip_content'] = "El número de serie del escáner se indica en una etiqueta ubicada en la parte posterior o inferior del escáner.";
$lang['emailCheck'] = "Compruebe su correo electrónico";
$lang['sent_link'] = "Hemos enviado un enlace a";
$lang['spam_mail'] = "Si no lo ve en 10 minutos, compruebe la carpeta de correo no deseado, correo basura u otros correos.";
$lang['visit1'] = "¿Todavía no lo ha recibido?";
$lang['visit2'] = "Visite nuestro Centro de asistencia";
$lang['model_Number'] = "Número de modelo";


/* media page */
$lang['heading1'] = "Podemos enviarle los medios físicos";
$lang['heading2'] = "para su escáner Kodak Alaris.";
$lang['heading3'] = "Solo tiene que decirnos dónde desea que los enviemos.";
$lang['email_optional'] = "Correo electrónico (opcional)";
$lang['Scanner_Serial_Number'] = "Número de serie del escáner";
$lang['findit'] = "Buscar";
//$lang['tooltip_title'] = "Suchen Sie nach Ihrer Seriennummer."; 
//$lang['tooltip_content'] = "Das Etikett mit Ihrer Seriennummer befindet sich auf der Rück- oder Unterseite Ihres Scanners."; 
$lang['companyname'] = "Nombre de la empresa";
$lang['placeholder1'] = "mi.nombre@ejemplo.com";
$lang['placeholder2'] = "Ejemplo SA.";
$lang['contactname'] = "Nombre de contacto";
$lang['placeholder3'] = "Nombre Apellido";
$lang['streetaddress'] = "Dirección postal";
$lang['placeholder4'] = "C/ Principal 123";
$lang['addressline2'] = "Línea de dirección 2 (opcional)";
$lang['placeholder5'] = "Bloque, edificio, piso";
$lang['city'] = "Ciudad";
$lang['state_province_region'] = "Comunidad/Provincia/Región";
$lang['country'] = "País";
$lang['postalcode'] = "Código postal";
$lang['send_me_disk'] = "Deseo recibir un disco";
$lang['prefer'] = "¿Prefiere descargar el instalador?";
$lang['note'] = "Deseo recibir notificaciones sobre actualizaciones de software y ofertas especiales.";
$lang['policy1'] = "Valoramos su privacidad.";
$lang['policy2'] = "Revise nuestra política de privacidad.";

/* Download Page */
$lang['choose_software'] = "Seleccione el software que desea descargar";
$lang['download'] = "Descargar";
$lang['link_expired'] = "El enlace ha caducado";
$lang['expire_days'] = "Los enlaces caducan en 3 días.";
$lang['new_link'] = "No se preocupe. Podemos enviarle un enlace para que realice";
$lang['request_again'] = "la solicitud de nuevo.";
$lang['send_link'] = "Deseo recibir un nuevo enlace";
$lang['or'] = "o";
$lang['different_email'] = "proporcionar una dirección de correo electrónico distinta.";
?>
