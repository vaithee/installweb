<?php

$lang = array();

$lang['head_lbl_1'] = "让我们安装您的全新";
$lang['head_lbl_2'] = "Kodak Alaris 扫描仪.";
$lang['head_cap'] = "我们只需要您的电子邮件和扫描仪序列号。.";
$lang['email'] = "电子邮件";
$lang['scanner_serial_no'] = "扫描仪序列号";
$lang['find_it'] = "查找它";
$lang['download_link'] = "请发送链接给我";
//$lang['physical_disk'] = "Besoin d'un disque physique?";
$lang['notify'] = "我们只需要您的电子邮件地址来满足您的请求";
$lang['privacy_policy_1'] = "我们非常重视您的隐私。.";
$lang['privacy_policy_2'] = "请查看我们的隐私政策";
$lang['placeholder_email'] = "my.name@example.com";
$lang['tooltip_title'] = "查找您的序列号.";
$lang['tooltip_content'] = "在扫描仪背面或底部可找到您的序列号标签。.";
$lang['emailCheck'] = "请查看您的电子邮件";
$lang['sent_link'] = "我们已发送链接至";
$lang['spam_mail'] = "如果您在 10 分钟内没有看到电子邮件，请检查您的垃圾邮件或待过滤的邮件文件夹。.";
$lang['visit1'] = "仍没有收到电子邮件？";
$lang['visit2'] = "请访问我们的支持中心。.";
$lang['model_Number'] = "型号";


/* media page */
$lang['heading1'] = "索取适用于您";
$lang['heading2'] = "Kodak Alaris 扫描仪的物理介质";
$lang['heading3'] = "只需告诉我们寄送地址。";
$lang['email_optional'] = "电子邮件（可选）";
$lang['Scanner_Serial_Number'] = "扫描仪序列号";
$lang['findit'] = "查找它";
//$lang['tooltip_title'] = "Suchen Sie nach Ihrer Seriennummer."; 
//$lang['tooltip_content'] = "Das Etikett mit Ihrer Seriennummer befindet sich auf der Rück- oder Unterseite Ihres Scanners."; 
$lang['companyname'] = "公司名称";
$lang['placeholder1'] = "my.name@example.com";
$lang['placeholder2'] = "示例，Inc.";
$lang['contactname'] = "联系人姓名";
$lang['placeholder3'] = "名字姓氏";
$lang['streetaddress'] = "街道地址";
$lang['placeholder4'] = "123 主街道";
$lang['addressline2'] = "地址行 2（可选";
$lang['placeholder5'] = "可选公寓、套房、单位、建筑物";
$lang['city'] = "城市";
$lang['state_province_region'] = "省/市/自治区";
$lang['country'] = "国家/地区";
$lang['postalcode'] = "邮政编码";
$lang['send_me_disk'] = "请寄送磁盘给我";
$lang['prefer'] = "首选下载安装程序？";
$lang['note'] = "请通知我软件更新和特价优惠。";
$lang['policy1'] = "我们非常重视您的隐私。";
$lang['policy2'] = "请查看我们的隐私政策。";


/* Download Page */
$lang['choose_software'] = "选择您要下载的软件";
$lang['download'] = "下载";
$lang['link_expired'] = "您的链接已过期";
$lang['expire_days'] = "链接将在 3 天后过期。";
$lang['new_link'] = "不用担心。我们会为发送一个新的链接以";
$lang['request_again'] = "再次请求";
$lang['send_link'] = "请发送一个新链接给我";
$lang['or'] = "或";
$lang['different_email'] = "要求发送至不同的电子邮件地址";
?>