<?php session_start(); ?>
<html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <table width="680" align="center">
            <tr>
                <td>
                    <table width="539" align="center" style="padding-top:40px;">
                        <tr>
                            <td>
                                <font style="font-size: 10px; color: rgb(204, 204, 204);font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">&#27492;&#28858;&#36899;&#25509;&#33267;&#24744; Kodak Alaris &#25475;&#25551;&#22120;&#36575;&#39636;&#30340;&#36899;&#32080;&#12290;.</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="550" align="center" style="padding-top: 70px;">
                        <tr>
                            <td>
                                <font style="font-size: 54px; color: rgb(0, 0, 0); font-weight: bold; line-height: 1.111;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">&#27492;&#28858; Kodak Alaris &#25552;&#20379;&#32102;&#24744;&#30340;&#36575;&#39636;.</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="539" style="padding-top:5px;" align="center">
                        <tr>
                            <td>
                                <font style="font-size: 22px; color: rgb(153, 153, 153);font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">&#37323;&#25918;&#24433;&#20687;&#33287;&#36039;&#35338;&#30340;&#21147;&#37327;&#12290;.</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="539" align="center" style="padding-top: 45px;">
                        <tr>
                            <td>
                                <font style="font-size: 22px;  color: rgb(0, 0, 0); line-height: 1.2;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">&#36879;&#36942;&#27492;&#36899;&#32080;&#20006;&#19979;&#36617;&#24744;&#30340;&#36575;&#39636;&#65292;&#20197;&#23436;&#25104;&#25475;&#25551;&#22120;&#23433;&#35037;&#12290;&#27492;&#36899;&#32080;&#26371;&#22312; 3 &#26085;&#20839;&#36942;&#26399;&#12290;.</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding-top: 45px;padding-bottom: 150px;">
                    <table width="220" align="center" >
                        <tr>
                            <td style="border-radius: 6px;background-color: #3384DC;text-align:center;padding-top: 12px;padding-bottom: 12px;cursor: pointer;">
                                <a href="$BASE_PATH/download.php?pid=$productId&sd=$startdate&ed=$enddate&eid=$emailId" style="text-decoration: none;color: #ffffff;font-weight:bold;line-height: 2;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">
                                    &#21462;&#24471;&#24744;&#30340;&#36575;&#39636;
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="680" align="center"  style="padding-top: 50px;padding-bottom: 80px;background-color:#000;">
                        <tr>
                            <td>
                                <table width="539" align="center">
                                    <tr>
                                        <td>
                                            <img src="$BASE_PATH/images/KAlogo.png"/>
                                        </td>
                                    </tr>
                                    <tr>	
                                        <td style="padding-top: 40px;">
                                            <font style="font-size: 12px; color: rgb(85, 85, 85);  line-height: 1.2;font-weight:bold;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">Kodak Alaris Inc. &#8722; Information Management, 2400 Mount Read Blvd. Rochester, NY 14615</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 10px;">
                                            <font style="font-size: 12px; color: rgb(85, 85, 85);  line-height: 1.2;font-weight:bold;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">&copy; 2015 Kodak Alaris Inc.<br/>
                                            Kodak &#30340;&#21830;&#27161;&#21644;&#29986;&#21697;&#21253;&#35037;&#20418;&#30001;&#20234;&#22763;&#26364;&#26607;&#36948;&#20844;&#21496;&#25480;&#27402;&#20351;&#29992;&#12290;.</font>
                                        </td>
                                    </tr>
                                </table>			
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
<?php $_SESSION['productid'] = $productId; ?>

