<?php

$lang = array();

$lang['head_lbl_1'] = "讓我們來安裝您的全新";
$lang['head_lbl_2'] = "Kodak Alaris 掃描器.";
$lang['head_cap'] = "我們只需要您的電子郵件地址和掃描器序號。.";
$lang['email'] = "電子郵件";
$lang['scanner_serial_no'] = "掃描器序號";
$lang['find_it'] = "尋找它";
$lang['download_link'] = "傳送連結給我";
//$lang['physical_disk'] = "Besoin d'un disque physique?";
$lang['notify'] = "我們只會將您的電子郵件地址用於履行您的要求";
$lang['privacy_policy_1'] = "我們重視您的隱私權。.";
$lang['privacy_policy_2'] = "檢視我們的隱私權政策";
$lang['placeholder_email'] = "my.name@example.com";
$lang['tooltip_title'] = "尋找您的序號";
$lang['tooltip_content'] = "在掃描器背面或底部即可找到您的序號標籤。.";
$lang['emailCheck'] = "檢查您的電子郵件";
$lang['sent_link'] = "我們已傳送連結至";
$lang['spam_mail'] = "如果您在 10 分鐘內沒收到信，請檢查您的垃圾郵件或待過濾郵件資料夾。.";
$lang['visit1'] = "還是沒收到信？";
$lang['visit2'] = "請造訪我們的支援中心。.";
$lang['model_Number'] = "型號";


/* media page */
$lang['heading1'] = "索取適用於您";
$lang['heading2'] = "Kodak Alaris 掃描器的實體媒體";
$lang['heading3'] = "只要告訴我們寄送地址即可。";
$lang['email_optional'] = "電子郵件地址（選填）";
$lang['Scanner_Serial_Number'] = "掃描器序號";
$lang['findit'] = "尋找它";
//$lang['tooltip_title'] = "Suchen Sie nach Ihrer Seriennummer."; 
//$lang['tooltip_content'] = "Das Etikett mit Ihrer Seriennummer befindet sich auf der Rück- oder Unterseite Ihres Scanners."; 
$lang['companyname'] = "公司名稱";
$lang['placeholder1'] = "my.name@example.com";
$lang['placeholder2'] = "範例， Inc.";
$lang['contactname'] = "聯絡人姓名";
$lang['placeholder3'] = "名字 姓氏";
$lang['streetaddress'] = "街道地址";
$lang['placeholder4'] = "123 Main Street";
$lang['addressline2'] = "地址行 2（選填）";
$lang['placeholder5'] = "寓號，室號，大樓";
$lang['city'] = "縣市";
$lang['state_province_region'] = "州/省/地區";
$lang['country'] = "國家";
$lang['postalcode'] = "郵遞區號";
$lang['send_me_disk'] = "寄送光碟給我";
$lang['prefer'] = "偏好下載安裝程式？";
$lang['note'] = "通知我軟體更新和特別優惠。";
$lang['policy1'] = "我們重視您的隱私權。";
$lang['policy2'] = "檢視我們的隱私權政策。";


/* Download Page */
$lang['choose_software'] = "選擇您要下載的軟體";
$lang['download'] = "下載";
$lang['link_expired'] = "您的連結已過期";
$lang['expire_days'] = "連結會在 3 日後過期。";
$lang['new_link'] = "別擔心。我們會傳送新連結以";
$lang['request_again'] = "再次要求";
$lang['send_link'] = "傳送新連結給我";
$lang['or'] = "或是";
$lang['different_email'] = "要求寄送至不同的電子郵件地址";
?>