<?php

$lang = array();

$lang['head_lbl_1'] = "신규 Kodak Alaris";
$lang['head_lbl_2'] = "스캐너 설치를 시작합니다.";
$lang['head_cap'] = "이메일 주소와 스캐너의 일련 번호만 있으면 모든 준비 완료입니다.";
$lang['email'] = "이메일";
$lang['scanner_serial_no'] = "스캐너 일련 번호";
$lang['find_it'] = "찾기";
$lang['download_link'] = "링크 보내기";
//$lang['physical_disk'] = "Besoin d'un disque physique?";
$lang['notify'] = "이메일 주소는 요청을 처리하기 위해서만 사용됩니다";
$lang['privacy_policy_1'] = "Kodak Alaris는 사용자의 개인 정보 보호를 준수합니다.";
$lang['privacy_policy_2'] = "개인 정보 보호 정책 확인.";
$lang['placeholder_email'] = "my.name@example.com";
$lang['tooltip_title'] = "일련 번호 찾기.";
$lang['tooltip_content'] = "스캐너의 후면 또는 하단에 일련 번호 라벨이 부착되어 있습니다.";
$lang['emailCheck'] = "이메일을 확인해 주십시오";
$lang['sent_link'] = "다음 주소로 링크를 발송했습니다";
$lang['spam_mail'] = "10분 이내에 수신 확인이 되지 않는 경우 스팸, 낮은 우선 순위 메일, 정크 폴더를 확인하십시오.";
$lang['visit1'] = "아직 수신되지 않았습니까?";
$lang['visit2'] = "지원 센터를 방문해 주십시오.";
$lang['model_Number'] = "모델 번호";


/* media page */
$lang['heading1'] = "Kodak Alaris 스캐너를";
$lang['heading2'] = "위한 물리 디스크를 보내드립니다.";
$lang['heading3'] = "받아보실 주소를 알려 주십시오.";
$lang['email_optional'] = "이메일(옵션)";
$lang['Scanner_Serial_Number'] = "스캐너 일련 번호";
$lang['findit'] = "찾기";
//$lang['tooltip_title'] = "Suchen Sie nach Ihrer Seriennummer."; 
//$lang['tooltip_content'] = "Das Etikett mit Ihrer Seriennummer befindet sich auf der Rück- oder Unterseite Ihres Scanners."; 
$lang['companyname'] = "회사명";
$lang['placeholder1'] = "my.name@example.com";
$lang['placeholder2'] = "XXX 회사";
$lang['contactname'] = "연락처 이름";
$lang['placeholder3'] = "이름 성";
$lang['streetaddress'] = "나머지 주소";
$lang['placeholder4'] = "123번지";
$lang['addressline2'] = "주소 2(옵션)";
$lang['placeholder5'] = "아파트, 건물 이름 등(옵션)";
$lang['city'] = "도시";
$lang['state_province_region'] = "시/도/지역";
$lang['country'] = "국가";
$lang['postalcode'] = "우편 번호";
$lang['send_me_disk'] = "디스크 보내기";
$lang['prefer'] = "설치 프로그램을 다운로드하시겠습니까?";
$lang['note'] = "소프트웨어 업데이트 및 스페셜 오퍼 소식을 받아보겠습니다.";
$lang['policy1'] = "Kodak Alaris는 사용자의 개인 정보 보호를 준수합니다.";
$lang['policy2'] = "개인 정보 보호 정책을 확인하십시오.";

/* Download Page */
$lang['choose_software'] = "다운로드할 소프트웨어 선택";
$lang['download'] = "다운로드";
$lang['link_expired'] = "링크 유효 기간이 만료되었습니다.";
$lang['expire_days'] = "링크는 3일 간 유효합니다.";
$lang['new_link'] = "안심하십시오. 신규 링크를 다시";
$lang['request_again'] = "보내드립니다.";
$lang['send_link'] = "신규 링크 전송";
$lang['or'] = "또는";
$lang['different_email'] = "다른 이메일 요청";
?>