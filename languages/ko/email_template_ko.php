<?php session_start(); ?>
<html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <table width="680" align="center">
            <tr>
                <td>
                    <table width="539" align="center" style="padding-top:40px;">
                        <tr>
                            <td>
                                <font style="font-size: 10px; color: rgb(204, 204, 204);font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">Kodak Alaris &#49828;&#52880;&#45320; &#49548;&#54532;&#53944;&#50920;&#50612; &#47553;&#53356;&#51077;&#45768;&#45796;.</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="550" align="center" style="padding-top: 70px;">
                        <tr>
                            <td>
                                <font style="font-size: 54px; color: rgb(0, 0, 0); font-weight: bold; line-height: 1.111;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">Kodak Alaris &#49548;&#54532;&#53944;&#50920;&#50612;&#51077;&#45768;&#45796;.</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="539" style="padding-top:5px;" align="center">
                        <tr>
                            <td>
                                <font style="font-size: 22px; color: rgb(153, 153, 153);font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">&#51060;&#48120;&#51648;&#50752; &#51221;&#48372;&#51032; &#55192;&#51012; &#54876;&#50857;&#54616;&#49901;&#49884;&#50724;.</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="539" align="center" style="padding-top: 45px;">
                        <tr>
                            <td>
                                <font style="font-size: 22px;  color: rgb(0, 0, 0); line-height: 1.2;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">&#47553;&#53356;&#47484; &#53364;&#47533;&#54616;&#44256; &#49548;&#54532;&#53944;&#50920;&#50612;&#47484; &#49444;&#52824;&#54616;&#47732; &#49828;&#52880;&#45320;&#51032; &#49444;&#52824;&#44032; &#50756;&#47308;&#46121;&#45768;&#45796;. &#47553;&#53356;&#45716; 3&#51068; &#44036; &#50976;&#54952;&#54633;&#45768;&#45796;.</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding-top: 45px;padding-bottom: 150px;">
                    <table width="220" align="center" >
                        <tr>
                            <td style="border-radius: 6px;background-color: #3384DC;text-align:center;padding-top: 12px;padding-bottom: 12px;cursor: pointer;">
                                <a href="$BASE_PATH/download.php?pid=$productId&sd=$startdate&ed=$enddate&eid=$emailId" style="text-decoration: none;color: #ffffff;font-weight:bold;line-height: 2;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">
                                    &#49548;&#54532;&#53944;&#50920;&#50612; &#48155;&#44592;
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="680" align="center"  style="padding-top: 50px;padding-bottom: 80px;background-color:#000;">
                        <tr>
                            <td>
                                <table width="539" align="center">
                                    <tr>
                                        <td>
                                            <img src="$BASE_PATH/images/KAlogo.png"/>
                                        </td>
                                    </tr>
                                    <tr>	
                                        <td style="padding-top: 40px;">
                                            <font style="font-size: 12px; color: rgb(85, 85, 85);  line-height: 1.2;font-weight:bold;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">Kodak Alaris Inc. &#8722; Information Management, 2400 Mount Read Blvd. Rochester, NY 14615</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 10px;">
                                            <font style="font-size: 12px; color: rgb(85, 85, 85);  line-height: 1.2;font-weight:bold;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">&copy; 2015 Kodak Alaris Inc.<br/>
                                            Kodak &#49345;&#54364;&#50752; &#53944;&#47112;&#51060;&#46300; &#46300;&#47112;&#49828;&#45716; Eastman Kodak Company&#47196;&#48512;&#53552; &#46972;&#51060;&#49468;&#49828;&#47484; &#48155;&#50500; &#49324;&#50857;&#46121;&#45768;&#45796;.</font>
                                        </td>
                                    </tr>
                                </table>			
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
<?php $_SESSION['productid'] = $productId; ?>

