<?php

$lang = array();

$lang['head_lbl_1'] = "Installazione del nuovo";
$lang['head_lbl_2'] = "scanner Kodak Alaris.";
$lang['head_cap'] = "Abbiamo bisogno del tuo indirizzo email e del numero di serie dello scanner.";
$lang['email'] = "Email";
$lang['scanner_serial_no'] = "Numero di serie scanner";
$lang['find_it'] = "Trovalo";
$lang['download_link'] = "Invia un collegamento";
//$lang['physical_disk'] = "Besoin d'un disque physique?";
$lang['notify'] = "Utilizziamo il tuo indirizzo email esclusivamente per completare la richiesta";
$lang['privacy_policy_1'] = "Diamo valore alla tua privacy.";
$lang['privacy_policy_2'] = "Leggi la nostra informativa sulla privacy.";
$lang['placeholder_email'] = "nome@esempio.com";
$lang['tooltip_title'] = "Trova il numero di serie.";
$lang['tooltip_content'] = "Cerca l'etichetta del numero di serie sul retro o nella parte inferiore dello scanner.";
$lang['model_Number'] = "Numero modello";
$lang['emailCheck'] = "Controlla l'email";
$lang['sent_link'] = "Abbiamo inviato un collegamento a";
$lang['spam_mail'] = "Se non visualizzi il collegamento entro 10 minuti, controlla le cartelle di posta indesiderata, messaggi secondari o cestino.";
$lang['visit1'] = "Non l'hai ancora ricevuto?";
$lang['visit2'] = "Visita il nostro centro assistenza";

/* media page */
$lang['heading1'] = "Ottieni file multimediali inviati per il tuo";
$lang['heading2'] = "scanner Kodak Alaris";
$lang['heading3'] = "Basta comunicarci dove inviarli.";
$lang['email_optional'] = "Email (opzionale)";
$lang['Scanner_Serial_Number'] = "Numero di serie scanner";
$lang['findit'] = "Trovalo";
//$lang['tooltip_title'] = "Suchen Sie nach Ihrer Seriennummer."; 
//$lang['tooltip_content'] = "Das Etikett mit Ihrer Seriennummer befindet sich auf der Rück- oder Unterseite Ihres Scanners."; 
$lang['companyname'] = "Nome azienda";
$lang['placeholder1'] = "nome@esempio.com";
$lang['placeholder2'] = "Azienda S.r.l.";
$lang['contactname'] = "Nome del contatto";
$lang['placeholder3'] = "Nome e cognome";
$lang['streetaddress'] = "Indirizzo";
$lang['placeholder4'] = "Nome via, 123";
$lang['addressline2'] = "Riga indirizzo 2 (opzionale)";
$lang['placeholder5'] = "Edificio, scala";
$lang['city'] = "Città";
$lang['state_province_region'] = "Stato/Provincia/Regione";
$lang['country'] = "Paese";
$lang['postalcode'] = "Codice postale";
$lang['send_me_disk'] = "Inviami un disco";
$lang['prefer'] = "Preferisci scaricare un programma di installazione?";
$lang['note'] = "Inviami notifiche relative a aggiornamenti software e offerte speciali.";
$lang['policy1'] = "Diamo valore alla tua privacy.";
$lang['policy2'] = "Leggi la nostra informativa sulla privacy.";

/* Download Page */
$lang['choose_software'] = "Scegli il software che desideri scaricare";
$lang['download'] = "Download";
$lang['link_expired'] = "Collegamento scaduto";
$lang['expire_days'] = "I collegamenti scadono fra 3 giorni.";
$lang['new_link'] = "Non ti preoccupare! Ti invieremo un nuovo collegamento per effettuare una";
$lang['request_again'] = "nuova richiesta";
$lang['send_link'] = "Inviami un nuovo collegamento";
$lang['or'] = "o";
$lang['different_email'] = "consentimi di fornire un indirizzo email differente";
?>