<?php

$lang = array();

$lang['head_lbl_1'] = "それでは、あなたの新しいをインストールしてみましょう";
$lang['head_lbl_2'] = "KODAK ALARISスキャナ";
$lang['head_cap'] = "私たちが必要とするすべてはあなたの電子メールやスキャナーのシリアル番号です。";
$lang['email'] = "Eメール";
$lang['scanner_serial_no'] = "スキャナのシリアル番号";
$lang['find_it'] = "それを見つける";
$lang['download_link'] = "私にリンクを送信";
$lang['physical_disk'] = "物理ディスクが必要ですか？";
$lang['notify'] = "私たちは、あなたが要求したサービスを提供するためにあなたの電子メールを使用します";
$lang['privacy_policy_1'] = "私たちはあなたのプライバシーを大切にしています。私たちを確認";
$lang['privacy_policy_2'] = "個人情報保護方針";
?>