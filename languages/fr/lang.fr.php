<?php

$lang = array();

$lang['head_lbl_1'] = "Installons votre nouveau";
$lang['head_lbl_2'] = "scanner Kodak Alaris";
$lang['head_cap'] = "Nous avons juste besoin de votre adresse e-mail et du numéro de série de votre scanner.";
$lang['email'] = "Adresse e-mail";
$lang['scanner_serial_no'] = "Numéro de série du scanner";
$lang['find_it'] = "Localiser";
$lang['download_link'] = "Envoyez-moi un lien";
$lang['physical_disk'] = "Besoin d'un disque physique?";
$lang['notify'] = "Nous utilisons uniquement votre adresse e-mail pour répondre à votre requête";
$lang['privacy_policy_1'] = "Nous apprécions votre vie privée. Consultez notre";
$lang['privacy_policy_2'] = "politique de confidentialité";
$lang['placeholder_email'] = "mon.nom@exemple.com";
$lang['tooltip_title'] = "Localiser votre numéro de série";
$lang['tooltip_content'] = "Localisez l'étiquette de votre numéro de série à l'arrière ou en-dessous de votre scanner.";
$lang['model_Number'] = "Numéro de modèle";
$lang['emailCheck'] = "Vérifiez votre boîte de réception";
$lang['sent_link'] = "Nous avons envoyé un lien à";
$lang['spam_mail'] = "Si vous ne l’avez pas reçu dans 10 minutes, vérifiez votre dossier de spam, de courrier pêle-mêle ou de courrier indésirable.";
$lang['visit1'] = "Vous ne l'avez toujours pas reçu ?";
$lang['visit2'] = " Visitez notre centre d'assistance.";


/* media page */
$lang['heading1'] = "Faites-vous envoyer un support physique";
$lang['heading2'] = "pour votre scanner Kodak Alaris";
$lang['heading3'] = "Indiquez-nous simplement où l'envoyer.";
$lang['email_optional'] = "Adresse e-mail (en option)";
$lang['Scanner_Serial_Number'] = "Numéro de série du scanner";
$lang['findit'] = "Localiser";
//$lang['tooltip_title'] = "Suchen Sie nach Ihrer Seriennummer."; 
//$lang['tooltip_content'] = "Das Etikett mit Ihrer Seriennummer befindet sich auf der Rück- oder Unterseite Ihres Scanners."; 
$lang['companyname'] = "Nom de la société";
$lang['placeholder1'] = "mon.nom@exemple.com";
$lang['placeholder2'] = "Exemple, Inc.";
$lang['contactname'] = "Nom du contact";
$lang['placeholder3'] = "Prénom nom de famille";
$lang['streetaddress'] = "Adresse";
$lang['placeholder4'] = "123 rue principale";
$lang['addressline2'] = "Adresse 2 (en option)";
$lang['placeholder5'] = "Appartement, suite, bloc, bâtiment";
$lang['city'] = "Ville";
$lang['state_province_region'] = "État/ province/ région";
$lang['country'] = "Pays";
$lang['postalcode'] = "Code postal";
$lang['send_me_disk'] = "Envoyez-moi un disque";
$lang['prefer'] = "Préférez-vous télécharger le programme d'installation ?";
$lang['note'] = "Veuillez m'informer des mises à jour logicielles et des offres spéciales.";
$lang['policy1'] = "Nous respectons votre vie privée.";
$lang['policy2'] = "Consultez notre politique de confidentialité.";


/* Download Page */
$lang['choose_software'] = "Choisissez le logiciel que vous voulez télécharger";
$lang['download'] = "Téléchargez";
$lang['link_expired'] = "Votre lien a expiré";
$lang['expire_days'] = "Les liens expirent après 3 jours.";
$lang['new_link'] = "Ne vous inquiétez pas. Nous pouvons vous envoyer un autre lien pour une";
$lang['request_again'] = "nouvelle requête";
$lang['send_link'] = "Envoyez-moi un nouveau lien";
$lang['or'] = "ou";
$lang['different_email'] = "demandez une adresse e-mail différente";
?>