<?php session_start(); ?>
<html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <table width="680" align="center">
            <tr>
                <td>
                    <table width="539" align="center" style="padding-top:40px;">
                        <tr>
                            <td>
                                <font style="font-size: 10px; color: rgb(204, 204, 204);font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">Here&rsquo;s the link to your Kodak Alaris scanner software.</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="550" align="center" style="padding-top: 70px;">
                        <tr>
                            <td>
                                <font style="font-size: 54px; color: rgb(0, 0, 0); font-weight: bold; line-height: 1.111;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">Here&rsquo;s your software from Kodak Alaris</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="539" style="padding-top:5px;" align="center">
                        <tr>
                            <td>
                                <font style="font-size: 22px; color: rgb(153, 153, 153);font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">Unlock the power of your images and information.</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="539" align="center" style="padding-top: 45px;">
                        <tr>
                            <td>
                                <font style="font-size: 22px;  color: rgb(0, 0, 0); line-height: 1.2;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">Follow the link and download your software to complete the installation of your scanner. The link expires in 3 days.</font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding-top: 45px;padding-bottom: 150px;">
                    <table width="220" align="center" >
                        <tr>
                            <td style="border-radius: 6px;background-color: #3384DC;text-align:center;padding-top: 12px;padding-bottom: 12px;cursor: pointer;">
                                <a href="$BASE_PATH/download.php?pid=$productId&sd=$startdate&ed=$enddate&eid=$emailId" style="text-decoration: none;color: #ffffff;font-weight:bold;line-height: 2;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">
                                    Get your software
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="680" align="center"  style="padding-top: 50px;padding-bottom: 80px;background-color:#000;">
                        <tr>
                            <td>
                                <table width="539" align="center">
                                    <tr>
                                        <td>
                                            <img src="$BASE_PATH/images/KAlogo.png"/>
                                        </td>
                                    </tr>
                                    <tr>	
                                        <td style="padding-top: 40px;">
                                            <font style="font-size: 12px; color: rgb(85, 85, 85);  line-height: 1.2;font-weight:bold;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">Kodak Alaris Inc. &#8722; Information Management, 2400 Mount Read Blvd. Rochester, NY 14615</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top: 10px;">
                                            <font style="font-size: 12px; color: rgb(85, 85, 85);  line-height: 1.2;font-weight:bold;font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;">&copy; 2015 Kodak Alaris Inc.<br/>
                                            The Kodak trademark and trade dress are used under license from Eastman Kodak Company.</font>
                                        </td>
                                    </tr>
                                </table>			
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
<?php $_SESSION['productid'] = $productId; ?>

