<?php

$lang = array();

$lang['head_lbl_1'] = "Let's install your new";
$lang['head_lbl_2'] = "Kodak Alaris scanner";
$lang['head_cap'] = "All we need is your email and scanner serial number.";
$lang['email'] = "Email";
$lang['scanner_serial_no'] = "Scanner Serial Number";
$lang['find_it'] = "Find it";
$lang['download_link'] = "Send me a link";
$lang['physical_disk'] = "Need a physical disk?";
$lang['notify'] = "We use your email only to fulfill your request";
$lang['privacy_policy_1'] = "We value your privacy. Review our";
$lang['privacy_policy_2'] = "privacy policy";
$lang['placeholder_email'] = "my.name@example.com";
$lang['tooltip_title'] = "Find your serial number.";
$lang['tooltip_content'] = "Find your serial number label on the back or bottom of your scanner.";
$lang['model_Number'] = "Model Number";
$lang['emailCheck'] = "Check your email";
$lang['sent_link'] = "We sent a link to";
$lang['spam_mail'] = "If you don't see it within 10 minutes, check your spam, clutter or junk folder.";
$lang['visit1'] = "Still didn't receive it?";
$lang['visit2'] = "Visit our Support Center.";

/* media page */
$lang['heading1'] = "Get physical media sent to you for your";
$lang['heading2'] = "Kodak Alaris scanner";
$lang['heading3'] = "Just tell us where to send it.";
$lang['email_optional'] = "Email (optional)";
$lang['Scanner_Serial_Number'] = "Scanner Serial Number";
$lang['findit'] = "Find it";
//$lang['tooltip_title'] = "Find your serial number"; 
//$lang['tooltip_content'] = "Find your serial number label on the back or bottom of your scanner."; 
$lang['companyname'] = "Company Name";
$lang['placeholder1'] = "my.name@example.com";
$lang['placeholder2'] = "Example, Inc.";
$lang['contactname'] = "Contact Name";
$lang['placeholder3'] = "First Last";
$lang['streetaddress'] = "Street Address";
$lang['placeholder4'] = "123 Main Street";
$lang['addressline2'] = "Address Line 2 (optional)";
$lang['placeholder5'] = "Optional Apt, suite, unit, building";
$lang['city'] = "City";
$lang['state_province_region'] = "State/Province/Region";
$lang['country'] = "Country";
$lang['postalcode'] = "Postal Code";
$lang['send_me_disk'] = "Send me a disk";
$lang['prefer'] = "Prefer to download the installer?";
$lang['note'] = "Notify me of software updates and special offers.";
$lang['policy1'] = "We value your privacy.";
$lang['policy2'] = "Review our privacy policy.";

/* Download Page */
$lang['choose_software'] = "Choose the software you want to download";
$lang['download'] = "Download";
$lang['link_expired'] = "Your link expired";
$lang['expire_days'] = "Links expire after 3 days.";
$lang['new_link'] = "Don't worry. We can send a new link to";
$lang['request_again'] = "request again";
$lang['send_link'] = "Send me a new link";
$lang['or'] = "or";
$lang['different_email'] = "request a different email";

/* Email Template */
$lang['para1'] = "Here’s the link to your Kodak.";
$lang['para2'] = "Here’s your software from Kodak Alaris";
$lang['para3'] = "Unlock the power of your images and information.";
$lang['para4'] = "Follow the link and download your software to complete the installation of your scanner. The link expires in 3 days.";
$lang['para5'] = "Get your software";
$lang['para6'] = "Kodak Alaris Inc. − Information Management, 2400 Mount Read Blvd. Rochester, NY 14615";
$lang['para7'] = "© 2016 Kodak Alaris Inc.";
$lang['para8'] = "The Kodak trademark and trade dress are used under license from Eastman Kodak Company.";
?>